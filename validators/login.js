let validator = require('validator');
let models = require('../models');
const bcrypt = require('bcrypt')

const validateCreateUserField = (errors, req) => {
	if(!validator.isEmail(req.body.email)){
		errors['error'] = 'Please use a valid email';
	}
	if(!validator.isAscii(req.body.password)){
		errors['error'] = 'Invalid characters in password please try another';
	}
} 

 exports.validateLogin = (errors, req) => {
 	return new Promise((resolve, reject) => {
 		validateCreateUserField(errors, req);
 		return models.admin.findOne({
		where: {
			email: req.body.email
		}
	}).then(usr => {
	if(!usr){
		errors["error"] = "Invalid Login Details"
	}
	else if(usr.password != req.body.password){
		errors["error"] = "Invalid Login Details"
	}
// 	}
// 	else if(usr.status == "blocked"){
// 		errors["error"] = "Account Suspended Please Contact Support"
// 	}
	resolve(errors)
 	})
	
})
}

 exports.validateLoginInvestor = (errors, req) => {
 	return new Promise((resolve, reject) => {
 		validateCreateUserField(errors, req);
 		return models.investor.findOne({
		where: {
			email: req.body.email
		}
	}).then(usr => {
	if(!usr){
		errors["error"] = "Invalid Login Details"
	}
	else if(!bcrypt.compareSync(req.body.password, usr.password)){
		errors["error"] = "Invalid Login Details"
	}
// 	}
// 	else if(usr.status == "blocked"){
// 		errors["error"] = "Account Suspended Please Contact Support"
// 	}
	resolve(errors)
 	})
	
})
}