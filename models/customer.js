'use strict';
module.exports = (sequelize, DataTypes) => {
	var customer = sequelize.define('customer', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			allowNull: false,
			primaryKey: true
		},
		name: {
		type: DataTypes.STRING
		},
		email: {
		type: DataTypes.STRING,
		unique: true
		},
		address: {
		type: DataTypes.STRING    
		},
		phone: {
		type: DataTypes.STRING
		},
		bal: {
		type: DataTypes.INTEGER,
		defaultValue: 0
		}
	});
    customer.sync()
        customer.associate = function(models) {
    customer.hasMany(models.purchase, {foreignKey: "customer_id", as: "purchases"})
    
  };
  
	return customer;
}