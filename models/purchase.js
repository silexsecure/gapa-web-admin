'use strict';
module.exports = (sequelize, DataTypes) => {
	var purchase = sequelize.define('purchase', {
		id: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		amount: {
		type: DataTypes.INTEGER
		},
		quantity: {
		type: DataTypes.INTEGER
		},
		status: {
		type: DataTypes.STRING
		},
		customer_name: {
		type: DataTypes.STRING
		},
		customer_phone: {
		type: DataTypes.STRING
		},
		type: {
		type: DataTypes.STRING
		},
		customer_id: {
    	  type: DataTypes.INTEGER,
            allowNull: false,
            references: {  
              model: 'customers',
              key: 'id'
            }
		},
		retailer_id: {
    	  type: DataTypes.UUID,
            allowNull: false,
            references: {  
              model: 'admins',
              key: 'id'
            }
		},
		
		
	});
    purchase.sync()
    purchase.associate = function(models) {
            purchase.hasMany(models.purchase_item, {foreignKey: "purchase_id", as: "items"})
    purchase.belongsTo(models.customer, {foreignKey: "customer_id", as: "customer"})
    purchase.belongsTo(models.admin, {foreignKey: "retailer_id", as: "retailer"})
  };
	return purchase;
}