'use strict';
module.exports = (sequelize, DataTypes) => {
	var search = sequelize.define('search', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			allowNull: false,
			primaryKey: true
		},
        name: {
        type: DataTypes.STRING
        },
        email: {
        type: DataTypes.STRING            
        },
        phone: {
        type: DataTypes.STRING            
        },
		search: {
		type: DataTypes.STRING
		},
		location: {
		type: DataTypes.STRING
		},
		ip: {
		type: DataTypes.STRING    
		}
	});
    search.sync()
    search.associate = function(models) {
  };
	return search;
}