'use strict';
module.exports = (sequelize, DataTypes) => {
	var purchase_item = sequelize.define('purchase_item', {
		id: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		amount: {
		type: DataTypes.INTEGER
		},
		quantity: {
		type: DataTypes.INTEGER
		},
	    purchase_id: {
    	  type: DataTypes.INTEGER,
            allowNull: false,
            references: {  
              model: 'purchases',
              key: 'id'
            }
		},
	    part_id: {
    	  type: DataTypes.UUID,
            allowNull: false,
            references: {  
              model: 'parts',
              key: 'id'
            }
		}
		
	});
    purchase_item.sync()
    purchase_item.associate = function(models) {
    purchase_item.belongsTo(models.purchase, {foreignKey: "purchase_id", as: "purchase"})
    purchase_item.belongsTo(models.part, {foreignKey: "part_id", as: "part"})
  };
	return purchase_item;
}