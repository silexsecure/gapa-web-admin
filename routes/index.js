var express = require('express');
var router = express.Router();
var bcrypt = require("bcrypt")
var jwt = require('jsonwebtoken')
const passport = require("passport")
const myPassport = require('../passport_setup')(passport)
var models = require("../models")
const {validateUser} = require("../validators/signup");
const {validateLogin} = require("../validators/login");
const {validateChange} = require("../validators/change");
var Sequelize = require("sequelize")

var {isLoggedInReq, isLoggedIn, noAuth, isVerified, already_verified} = require("../middleware/hasAuth")

var userCont = require("../controllers/userCont")
// users routes 


/* GET home page. */
router.get('/', noAuth, userCont.show_login);

router.get('/dashboard', isLoggedIn, userCont.show_dashboard);

router.get("/users", isLoggedIn, userCont.show_users)

router.get("/searches", isLoggedIn, userCont.get_searches)

router.get("/admins", isLoggedIn, userCont.get_admins)

router.get("/withdraws", isLoggedIn, userCont.get_withdraws)

router.get("/images", isLoggedIn, userCont.get_img)

router.get("/create-product", isLoggedIn, userCont.show_addProduct)

router.get("/products", isLoggedIn, userCont.show_products)

router.get("/mail", isLoggedIn, userCont.get_send)

router.get("/makes", isLoggedIn, userCont.show_makes)

router.get("/cars", isLoggedIn, userCont.show_cars)

router.get("/car-models", isLoggedIn, userCont.show_models)

router.get("/categories", isLoggedIn, userCont.show_cats)

router.get("/makers", isLoggedIn, userCont.show_makers)

router.get("/orders", isLoggedIn, userCont.show_orders)

router.get("/orders/:id", isLoggedIn, userCont.show_order)

router.get("/products/:id", isLoggedIn, userCont.show_product)

router.get("/users/:id", isLoggedIn, userCont.show_user)

router.get("/categories/:cat", isLoggedIn, userCont.show_catprods)

router.get("/featured", isLoggedIn, userCont.get_featured)

router.get("/points", isLoggedIn, userCont.get_points)

router.get("/tops", isLoggedIn, userCont.get_tops)

router.get("/contact", isLoggedIn, userCont.get_settings)

router.get("/tc", isLoggedIn, userCont.get_tc)

router.get("/privacy", isLoggedIn, userCont.get_privacy)

router.get("/about", isLoggedIn, userCont.get_about)
//login 
router.post("/login", userCont.login)

router.post("/create-product", isLoggedInReq, userCont.create_product)

router.post("/create-make", isLoggedInReq, userCont.create_make)

router.post("/create-maker", isLoggedInReq, userCont.create_maker)

router.post("/create-cat", isLoggedInReq, userCont.create_cat)

router.post("/create-car", isLoggedInReq, userCont.create_car)

router.post("/create-model", isLoggedInReq, userCont.create_model)

router.post("/search-parts", userCont.search_products)

router.post("/find-by-code", userCont.find_by_code)

router.post("/edit-car", isLoggedInReq, userCont.edit_car)

router.post("/edit-car-2", isLoggedInReq, userCont.edit_car_2)

router.post("/edit-model", isLoggedInReq, userCont.edit_model)

router.post("/edit-model-2", isLoggedInReq, userCont.edit_model_2)

router.post("/edit-cat", isLoggedInReq, userCont.edit_cat)

router.post("/edit-cat-2", isLoggedInReq, userCont.edit_cat_2)

router.post("/edit-maker", isLoggedInReq, userCont.edit_maker)

router.post("/edit-maker-2", isLoggedInReq, userCont.edit_maker_2)

router.post("/edit-brand", isLoggedInReq, userCont.edit_brand)

router.post("/edit-brand-2", isLoggedInReq, userCont.edit_brand_2)

router.post("/add-top", isLoggedInReq, userCont.add_top)

router.post("/edit-top", isLoggedInReq, userCont.edit_top)

router.post("/add-featured", isLoggedInReq, userCont.add_featured)

router.post("/remove-featured", isLoggedInReq, userCont.remove_featured)

router.post("/create-contact", isLoggedInReq, userCont.create_contact)

router.post("/edit-contact", isLoggedInReq, userCont.edit_contact)

router.post("/edit-part-1", isLoggedInReq, userCont.edit_prod_1)

router.post("/edit-part-2", isLoggedInReq, userCont.edit_prod_2)

router.post("/update-status", isLoggedInReq, userCont.update_status)

router.post("/create-tc", isLoggedInReq, userCont.create_tc)

router.post("/edit-tc", isLoggedInReq, userCont.edit_tc)

router.post("/create-privacy", isLoggedInReq, userCont.create_privacy)

router.post("/edit-privacy", isLoggedInReq, userCont.edit_privacy)

router.post("/create-about", isLoggedInReq, userCont.create_about)

router.post("/edit-about", isLoggedInReq, userCont.edit_about)

router.post("/add-logistic", isLoggedInReq, userCont.add_logistic)

router.post("/decline-withdrawal", isLoggedInReq, userCont.decline_withdraw)

router.post("/confirm-withdrawal", isLoggedInReq, userCont.accept_withdraw)

router.post("/update-points", isLoggedInReq, userCont.update_points)

router.post("/logo", isLoggedInReq, userCont.update_logo)

router.post("/slider-1", isLoggedInReq, userCont.update_slider_1)

router.post("/slider-2", isLoggedInReq, userCont.update_slider_2)

router.post("/slider-3", isLoggedInReq, userCont.update_slider_3)

router.post("/send-mail", isLoggedInReq, userCont.send_mail)

router.post("/send-mail", isLoggedInReq, userCont.send_mail)

router.post("/send-mail", isLoggedInReq, userCont.send_mail)

router.post("/send-mail", isLoggedInReq, userCont.send_mail)

router.post("/send-mail", isLoggedInReq, userCont.send_mail)

router.post("/delete-brand", isLoggedInReq, userCont.delete_brand)

router.post("/delete-car", isLoggedInReq, userCont.delete_car)

router.post("/delete-model", isLoggedInReq, userCont.delete_model)

router.post("/delete-maker", isLoggedInReq, userCont.delete_maker)

router.post("/delete-product", isLoggedInReq, userCont.delete_prod)

router.post("/toggle-coming", isLoggedInReq, userCont.toggle_coming)

router.get("/logout", (req, res, next) => {
    	req.logout();
	req.session.destroy()
	res.redirect('/')
})



module.exports = router;
