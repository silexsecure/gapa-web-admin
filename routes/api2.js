var express = require('express');
var router = express.Router();
var bcrypt = require("bcrypt")
var jwt = require('jsonwebtoken')
const passport = require("passport")
const myPassport = require('../passport_setup')(passport)
var models = require("../models")
const {validateUser} = require("../validators/signup");
const {validateLogin} = require("../validators/login");
const {validateChange} = require("../validators/change");
var Sequelize = require("sequelize")
var {verify_token, verify} = require("../middleware/api_auth")

var api = require("../controllers/api2")
// api routes 

router.post('/login', api.login);

router.post('/register', api.register);

router.post("/verify", api.verify)

router.get('/top-products', api.get_top);

router.get('/featured-products', api.get_featured);

router.get('/categories', api.get_cats);

router.get('/brands', api.get_brands);

router.get('/cars', api.get_cars);

router.get('/models', api.get_models);

router.get('/products', api.get_products);

router.get('/me', verify, api.get_user);

router.post('/cart-items', api.get_items);

router.post('/product', api.get_product);

router.post('/order', verify, api.order);

router.post("/order-details", verify, api.get_order)

router.get("/orders", verify, api.get_orders)

router.get("/withdrawals", verify, api.get_withdrawals)

router.post("/request-withdrawal", verify, api.request_withdrawal)

router.post("/create-purchase", verify, api.create_purchase)

router.get("/my-purchases", verify, api.get_my_purchases)

router.post("/create-purchase", verify, api.create_purchase)

router.get("/customers", api.get_customers)

router.get("/custs", api.get_custs)

router.get("/online", verify, api.get_online_purchases)

router.get("/offline", verify, api.get_offline_purchases)

router.get("/my-customers", api.get_my_customers)

router.get("/users", verify, api.get_users)

router.get("/retailers", verify, api.get_retailers)

router.get("/retailer", verify, api.get_retailer)

router.post("/create-retailer", verify, api.create_retailer)

router.post("/update-profile", verify, api.update_profile)

router.post("/purchase", verify, api.get_purchase)


router.post("/forgot-password", api.forgot_password)

router.post("/reset-password", api.reset_password)


module.exports = router;
