const cryptoRandomString = require('crypto-random-string');
var bcrypt = require("bcrypt")
var jwt = require('jsonwebtoken')
const passport = require("passport")
const myPassport = require('../passport_setup')(passport)
var models = require("../models")
var Sequelize = require("sequelize")
const {isEmpty} = require('lodash')
const nodemailer = require("nodemailer")
const {validateUser, validateInvestor} = require("../validators/signup");
const {validateLogin, validateLoginInvestor} = require("../validators/login")
const {validateChange} = require("../validators/change")
const multer = require('multer');
const path = require('path');
const fs = require('fs'); 
const formidable = require('formidable');
var moment = require("moment")
const cron =  require("node-cron")
var {mail} = require("./mail")
const AfricasTalking = require('africastalking');
var util = require('util');
var logFile = fs.createWriteStream('log.txt', { flags: 'a' });
  // Or 'w' to truncate the file every time the process starts.
var logStdout = process.stdout;

console.log = function () {
  logFile.write(util.format.apply(null, arguments) + '\n');
  logStdout.write(util.format.apply(null, arguments) + '\n');
}
console.error = console.log;
// TODO: Initialize Africa's Talking
const africastalking = AfricasTalking({
  apiKey: '795bbe4af0239786441268f6c723950ca10f1408e66a3775d6e8b61d6a86b85a', 
  username: 'century21'
});
async function sendSMS(num, msg) {
    
    try {
  const result = await africastalking.SMS.send({
    to: num, 
    message: msg
  });
  console.log(result);
} catch(ex) {
  console.error(ex);
} 
};
var cur = "&#8358;"
const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './uploads/');
    },

    // By default, multer removes file extensions so let's add them back
    filename: function(req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    }
});

        const imageFilter = function(req, file, cb) {
    // Accept images only
    if (!file.originalname.match(/\.(jpg|JPG|jpeg|JPEG|png|PNG|gif|GIF)$/)) {
        req.fileValidationError = 'Only image files are allowed!';
        return cb(new Error('Only image files are allowed!'), false);
    }
    cb(null, true);
    };
    // 'profile_pic' is the name of our file input field in the HTML form
    let upload = multer({ storage: storage, fileFilter: imageFilter, limits: { fileSize: 2 * 1000 * 1000 }}).single('profile_pic'); 
    
    
const sendMail = (email, subject, page) => {
const transporter = nodemailer.createTransport({sendmail: true}, {
  from: "SUPPORT@GAPANAIJA.com",
  to: email,
  subject: subject,
});
transporter.sendMail({html: page}, (error, info) => {
     if (error) {
	console.log(error);
  } else {
    console.log('Email sent: ' + info.response);
  }
})
}


exports.login = (req, res, next) => {
const email = req.body.email
const password = req.body.password

if(!email || !password) {

	res.status(400).json({
    error: "Incomplete Details provided"
    })

}

else {
	
	models.admin.findOne({where: {email: req.body.email}}).then(user => {
    	if(user){

        const user_data = {
        id: user.id,
        first_name: user.firstName,
        last_name: user.lastName,
        profile_image: "https://gapaautoparts.com" + user.img_url,
        phone_number: user.phone,
        state: user.state,
        address: user.address,
        email: user.email
        }
       
        if(req.body.password === user.password){
        const token = jwt.sign({user: user_data}, "eLFQivL/gdEiW0iuZY1YgdYfF1U=@!2")
        res.json({
        message: "Signed in Successfully",
        token: token,
        user_data: user_data
        })
        }
        else {
        res.status(401).json({
        error: "Incorrect Login Details"
        })
        }
        }
    	else {
        res.status(401).json({
        	error: "User Account Not found"
        })
        }
    })
	
}
}

exports.register = (req, res, next) => {
     if(!req.body.email || !req.body.password || !req.body.fname || !req.body.lname || !req.body.phone || !req.body.address){
     res.json({error: "Incomplete details provided"})   
    }
else{
                 var nums = req.body.phone.split("")
                                 console.log(nums)
                                if(nums[0] == "+"){
                                if(nums[0] == "+" && nums[1] == "2" && nums[2] == "3" ){
                                    var phn = req.body.phone
                                }
                                else {
                                    
                                    var phn = req.body.phone
                                    
                                }
                                
                                }else if(nums[0] == "0") {
                                    nums.shift()
                                
                                nums.unshift("+","2","3","4")
                                var phn = nums.join('')
                                     console.log(phn)
                                }
                                            if(req.body.ref){
                models.user.findOne({where: {referral_code: req.body.ref}}).then(reff => {
                    if(reff){
                        models.user.update({
                            bal: reff.bal + 2
                        }, {where: {
                            id: reff.id
                        }})
                    }
                })
            }
                 
			const hash = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(10));
			var token = cryptoRandomString({length: 6, type: 'numeric'})
			                    
			                            var data = {
                    			    	firstName: req.body.fname,
                    			    	lastName: req.body.lname,
                    			        phone: phn,
                        				email: req.body.email,
                        				password: hash,
                        				token: token,
                        				img_url: "/img/profile.png",
                        				address: req.body.address,
                        				referral_code: cryptoRandomString({length: 4, type: 'distinguishable'}) + "-" + cryptoRandomString({length: 4, type: 'numeric'})
                    			        }
                                let errors = {};
                        	return validateUser(errors, req).then(errors => {
                        		if(!isEmpty(errors)){
                        			res.status(400).json(errors)
                        		}
                        		
                        		
                        		else{
                        		  
                        			      return models.user.create(data).then(user => {
                        		            	if(user){
                        		            	   console.log(user);
                                                     	res.json({
                                                     	    message: "Account created successfully, Please verify your account"
                                                     	})
                                                        sendMail(user.email, "Email verification", `your verification token is ${user.token}`)
                                                        sendSMS(user.phone, `Hi ${user.firstName}, You have successfully signed up to Gapaautoparts. Your verification token is ${user.token}`)
                        			
                        			}
                        
                        			})
                        			.catch(err => {
                        
                        				if(err){
                        					console.log(err);
                        					res.status(500).json({error: "there was an error", err})
                        				}
                        
                        			})	  
                        		}
                        		
                        		
                        		})



		
		}
			



}

exports.verify = (req, res, next) => {
        if(!req.body.token){
       res.json({error: "Incomplete details provided"})
   }
   else{
       models.user.findOne({where: {token: req.body.token}}).then(usr => {
           if(!usr){
               res.status(400).json({error: "Invalid token provided"})
           }else if(usr){
               models.user.update({emailVerified: true, token: "******"}, {where: {id: usr.id}}).then((rows) => {
                   if(rows){
                       res.json({message: "email verification successful"})
                   }
                   else{
                       res.status(400).json({error: "an error occurred please try again"})
                   }
               })
           }
       })
       
       
       
   }
}

exports.get_top = (req, res, next) => {
    models.top.findAll({include: ["part"]}).then(tops => {
        var top_products = []
        tops.forEach(top => {
            top_products.push(top.part)
        })
        res.json(top_products)
    })
}

exports.get_featured = (req, res, next) => {
        models.featured.findAll({include: ["part"]}).then(tops => {
        var featured_products = []
        tops.forEach(top => {
            featured_products.push(top.part)
        })
        res.json(featured_products)
    })
}


exports.get_cats = (req, res, next) => {
    models.cat.findAll().then(cats => {
        var categories = []
        cats.forEach(cat => {
            categories.push({
                id: cat.id,
                title: cat.title,
                img_url: cat.img_url,
                createdAt: cat.createdAt
            })
        })
        res.json(categories)
    })
}

exports.get_brands = (req, res, next) => {
    models.brand.findAll().then(brands => {
        res.json(brands)
    })
}

exports.get_cars = (req, res, next) => {
   if(req.query.brand_id){
       models.brand.findOne({where: {id: req.query.brand_id}}).then(brand => {
           if(brand){
               
       models.car.findAll({where: {brand_id: req.query.brand_id}}).then(cars => {
           res.json({
               brand,
               cars
           })
       })
           }else {
               res.status(404).json({
                   error: "Invalid BRAND ID"
               })
           }
       })
   }else {
       res.status(400).json({
           error: "Please Provide BRAND ID"
       })
   }
}

exports.get_models = (req, res, next) => {
   if(req.query.car_id){
       models.car.findOne({where: {id: req.query.car_id}}).then(car => {
           if(car){
               
       models.model.findAll({where: {car_id: req.query.car_id}}).then(models => {
           res.json({
               car,
               models
           })
       })
           }else {
               res.status(404).json({
                   error: "Invalid Car ID"
               })
           }
       })
   }else {
       res.status(400).json({
           error: "Please Provide Car ID"
       })
   }
}

exports.get_products = (req, res, next) => {
     models.part.findAll({order: [["createdAt", "DESC"]], include: [{association: "model", as: "model", include: [{association: "car", as: "car", include: ["brand"]}]}]}).then(prods => {

        res.json({
            products: prods
        })
           })

}

exports.get_product = (req, res, next) => {
    if(req.body.id){
            models.part.findOne({where: {product_code: req.body.id}, include: ["maker", {association: "model", as: "model", include: [{association: "car", as: "car", include: ["brand"]}]}, {association: "reviews", as: "reviews", include: ["user"]}]}).then(part => {
            if(part){
                     
            res.json({
                product: part
            })
            
                 
        }else {
        res.status(404).send("No Product found with this ID")
        }
    })      
    }else {
        res.status(400).json({
            error: "Please Provide Product ID"
        })
    }
}


exports.get_user = (req, res, next) => {
     jwt.verify(req.query.token, 'eLFQivL/gdEiW0iuZY1YgdYfF1U=@!2', (err, data) => {
    if(err){
    res.status(400).json({
    error: "Please ensure you've provided a valid token"
    })
    }
    
    else {
    models.admin.findOne({where: {id: data.user.id}}).then(user => {
        models.purchase.count().then(p_count => {
            models.part.count().then(part_count => {
                models.order.count().then(order_count => {
                            if(user.role == "retail"){
            models.purchase.count({where: {retailer_id: user.id}}).then(cnt => {
            models.purchase.findAll({where: {retailer_id: user.id}}).then(purchases => {
                var months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
                var data1 = []
                var data2 = []
                months.forEach((month, id) => {
                    var amnt = 0
                    var length = purchases.filter(purchase => moment(purchase.createdAt).month() + 1 == month).length
                    purchases.forEach(p => {
                     if(moment(p.createdAt).month() + 1 == month){
                         amnt += Math.floor(p.amount/1000)
                     }  
                    })
                    data1.push(length)
                    data2.push(amnt)
                })
                var year = 0
                var month = 0
                var day = 0
                var total = 0
                purchases.forEach(pr => {
                if(moment(pr.createdAt).month() == moment().month()){
                         month += pr.amount
                     }
                                if(moment(pr.createdAt).year() == moment().year() ){
                         year += pr.amount
                     }
                if(moment(pr.createdAt).date() == moment().date()){
                         day += pr.amount
                         total += 1
                     }
                })
                
                   res.json({
            user: {
                name: user.name,
                email: user.email,
                role: user.role,
                image: user.img_url,
                total,
                p_count: cnt,
                order_count,
                part_count,
                series: [
                 {
                     name: "Sales",
                     data: data1
                 }
                ],
                series2: [
                 {
                     name: "Revenue",
                     data: data2
                 }
                ],
                year,
                month,
                day
            }
        })
            })
})
                            }else {
                    models.purchase.findAll().then(purchases => {
                models.order.findAll().then(orders => {
                    
                
                var months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
                var data1 = []
                var data2 = []
                var data3 = []
                var data4 = []
                months.forEach((month, id) => {
                    var amnt = 0
                    var amnt2 = 0
                    var length = purchases.filter(purchase => moment(purchase.createdAt).month() + 1 == month).length
                    purchases.forEach(p => {
                     if(moment(p.createdAt).month() + 1 == month){
                         amnt += Math.floor(p.amount/1000)
                     }  
                    })
                    var length2 = orders.filter(purchase => moment(purchase.createdAt).month() + 1 == month).length
                    orders.forEach(p => {
                     if(moment(p.createdAt).month() + 1 == month){
                         amnt2 += Math.floor(p.total_paid/1000)
                     }  
                    })
                    data1.push(length)
                    data2.push(amnt)
                    data3.push(length2)
                    data4.push(amnt2)
                })
                var year = 0
                var month = 0
                var day = 0
                var total = 0
                var year2 = 0
                var month2 = 0
                var day2 = 0
                purchases.forEach(pr => {
                if(moment(pr.createdAt).month() == moment().month()){
                         month += pr.amount
                     }
                                if(moment(pr.createdAt).year() == moment().year() ){
                         year += pr.amount
                     }
                if(moment(pr.createdAt).date() == moment().date()){
                         day += pr.amount
                         
                     }
                })
                orders.forEach(pr => {
                if(moment(pr.createdAt).month() == moment().month()){
                         month2 += pr.total_paid
                     }
                                if(moment(pr.createdAt).year() == moment().year() ){
                         year2 += pr.total_paid
                     }
                if(moment(pr.createdAt).date() == moment().date()){
                         day2 += pr.total_paid
                         
                     }
                })
                res.json({
            user: {
                name: user.name,
                email: user.email,
                role: user.role,
                image: user.img_url,
                total,
                p_count,
                order_count,
                part_count,
                series: [
                 {
                     name: "Sales",
                     data: data1
                 }
                ],
                series2: [
                 {
                     name: "Revenue",
                     data: data2
                 }
                ],
                series3: [
                 {
                     name: "Sales",
                     data: data3
                 }
                ],
                series4: [
                 {
                     name: "Revenue",
                     data: data4
                 }
                ],
                year,
                month,
                day,
                year2,
                month2,
                day2
            }
        })
})
            })
            
                   
        }  
                })
            })
        })

    })
    }
    })
}

exports.get_items = (req, res, next) => {
    if(req.body.products.length > 0){
        var items = []
        req.body.products.forEach((product, id, arr) => {
        if(id + 1 == arr.length){    
    models.part.findOne({where: {id: product.id}, include: [{association: "model", as: "model", include: [{association: "car", as: "car", include: ["brand"]}]}]}).then(item => {
        if(item){
        items.push(item)
        res.json({
            items
        })
        }
        else {
            res.json({
                error: "No Items"
            })
        }
        })
        }else {
             models.part.findOne({where: {id: product.id}, include: [{association: "model", as: "model", include: [{association: "car", as: "car", include: ["brand"]}]}]}).then(item => {
        if(item){
        items.push(item)
        }
        })   
        }
        })
    }else {
        res.json({
            items: []
        })
    }
}

exports.order = (req, res, next) => {
     jwt.verify(req.query.token, 'eLFQivL/gdEiW0iuZY1YgdYfF1U=@!2', (err, data) => {
    if(err){
    res.status(400).json({
    error: "Please ensure you've provided a valid token"
    })
    }
    
    else {
    models.user.findOne({where: {id: data.user.id}}).then(user => {
    if(user){
           if(req.body.items && req.body.items.length > 0 && req.body.payment_id && req.body.type  && req.body.amount && req.body.comment){
       
        if(req.body.type == "delivery"){
                    models.order.create({user_id:  user.id, state: null, delivery_address: req.body.address || "null", total_paid: req.body.amount, type: "delivery", status: "pending", txn_id: req.body.payment_id, commment: req.body.comment, shipping_cost: req.body.ship}).then(order => {
                       req.body.items.forEach((item, id)=> {
                        if(id + 1 == req.body.items.length){
                             models.item.create({order_id: order.id, part_id: item.id, amount: item.amount, quantity: item.quantity}).then(itm => {
                                 res.json({
                                     message: "order placed successfully",
                                     order: order
                                 })
                             })
                        }else {
                           models.item.create({order_id: order.id, part_id: item.id, amount: item.amount, quantity: item.quantity})
                        }
                            })
                        })
                }
                if(req.body.type == "pickup"){
                    models.order.create({user_id: user.id, state: null, delivery_address: req.body.address || "null", total_paid: req.body.amount, type: "pickup", status: "pending", txn_id: req.body.payment_id, commment: req.body.comment, shipping_cost: req.body.ship}).then(order => {
                       req.body.items.forEach((item, id)=> {
                        if(id + 1 == req.body.items.length){
                             models.item.create({order_id: order.id, part_id: item.id, amount: item.amount, quantity: item.quantity}).then(itm => {
                                 res.json({
                                     message: "order placed successfully",
                                     order: order
                                 })
                             })
                        }else {
                           models.item.create({order_id: order.id, part_id: item.id, amount: item.amount, quantity: item.quantity})
                        }
                            })
                        })
                }                
       
    }else {
        res.json({
            error: "Please Provide full details"
        })
    } 
    }else {
        res.status(400).json({
            error: "Invalid Token Provided"
        })
    }
    })
    }
    })

}

exports.get_withdrawals = (req, res, next) => {
    jwt.verify(req.query.token, 'eLFQivL/gdEiW0iuZY1YgdYfF1U=@!2', (err, data) => {
    if(err){
    res.status(400).json({
    error: "Please ensure you've provided a valid token"
    })
    }
    
    else {
    models.user.findOne({where: {id: data.user.id}}).then(user => {
        if(user){
             models.withdraw.findAll({where: {user_id: user.id}}).then(withdraws => {
                res.json(({
                    history: withdraws
                }))  
            })
        }else {
            res.status(400).json({
                error: "Invalid Token Provided"
            })
        }
    })
    }
    })
}

exports.get_orders = (req, res, next) => {
    jwt.verify(req.query.token, 'eLFQivL/gdEiW0iuZY1YgdYfF1U=@!2', (err, data) => {
    if(err){
    res.status(400).json({
    error: "Please ensure you've provided a valid token"
    })
    }
    
    else {
    models.user.findOne({where: {id: data.user.id}}).then(user => {
        if(user){
             models.order.findAll({where: {user_id: user.id}, include: [{association: "items", as: "items", include: [{association: "part", as: "part", include: [{association: "model", as: "model", include: ["car"]}]}]}]}).then(orders => {
                res.json(({
                    orders: orders
                }))  
            })
        }else {
            res.status(400).json({
                error: "Invalid Token Provided"
            })
        }
    })
    }
    })
}

exports.get_order = (req, res, next) => {
    jwt.verify(req.query.token, 'eLFQivL/gdEiW0iuZY1YgdYfF1U=@!2', (err, data) => {
    if(err){
    res.status(400).json({
    error: "Please ensure you've provided a valid token"
    })
    }
    
    else {
        if(req.body.order_id){
             models.order.findOne({where: {id: req.body.order_id}, include: [{association: "items", as: "items", include: [{association: "part", as: "part", include: [{association: "model", as: "model", include: ["car"]}]}]}]}).then(order => {
                res.json(({
                    order: order
                }))  
            })
        }else {
            res.status(400).json({
                error: "Please provide order ID"
            })
        }
    }
    })
}
exports.create_purchase = (req, res, next) => {
    jwt.verify(req.query.token, 'eLFQivL/gdEiW0iuZY1YgdYfF1U=@!2', (err, data) => {
    if(err){
    res.status(400).json({
    error: "Please ensure you've provided a valid token"
    })
    }
    
    else {
         models.admin.findOne({where: {id: data.user.id}}).then(user => {
    if(user){
           if(req.body.items && req.body.items.length > 0 && req.body.address && req.body.name  && req.body.amount && req.body.phone && req.body.type){
            models.customer.findOne({where: {phone: req.body.phone}}).then(cust => {
                if(cust){
                    models.customer.update({name: req.body.name, phone: req.body.phone, address: req.body.address, bal: 0}, {where: {id: cust.id}})
                     models.purchase.create({retailer_id: user.id, customer_id: cust.id, amount: req.body.amount, quantity: req.body.items.length, status: "paid", customer_name: cust.name, customer_phone: cust.phone, customer_id: cust.id, type: req.body.type}).then(order => {
                       req.body.items.forEach((item, id)=> {
                        models.part.findOne({where: {id: item.id}}).then(part => {
                            var partId = part.id
                        if(id + 1 == req.body.items.length){
                             models.purchase_item.create({purchase_id: order.id, part_id: item.id, amount: item.total, quantity: item.quantity}).then(itm => {
                                
                                 models.part.update({price: parseInt(part.price),stock: parseInt(part.stock) - parseInt(item.quantity)},{where: {id: partId}}).then(rows => {
                                     console.log(rows)
                                 res.json({
                                     message: "Purchase Added successfully",
                                     purchase: order
                                 })
                                 })
                             })
                        }else {
                           models.purchase_item.create({purchase_id: order.id, part_id: item.id, amount: item.total, quantity: item.quantity})
                           models.part.update({stock: parseInt(part.stock) - parseInt(item.quantity)},{where: {id: partId}})
                        }
                        })
                        
                            })
                        })
                }else {
                    models.customer.create({name: req.body.name, phone: req.body.phone, address: req.body.address, bal: 0}).then(cust => {
                        models.purchase.create({retailer_id: user.id, customer_id: cust.id, amount: req.body.amount, quantity: req.body.items.length, status: "paid", customer_name: cust.name, customer_id: cust.id, customer_phone: cust.phone, type: req.body.type}).then(order => {
                       req.body.items.forEach((item, id)=> {
                            models.part.findOne({where: {id: item.id}}).then(part => {
                                var partId = part.id
                        if(id + 1 == req.body.items.length){
                             models.purchase_item.create({purchase_id: order.id, part_id: item.id, amount: item.total, quantity: item.quantity}).then(itm => {
                   
                        models.part.update({price: parseInt(part.price), stock: 10},{where: {id: partId}}).then(rows => {
                            console.log(rows)
                            res.json({
                                     message: "Purchase Added successfully",
                                     purchase: order
                                 })
                        })
                                 
                             })
                        }else {
                           models.purchase_item.create({purchase_id: order.id, part_id: item.id, amount: item.total, quantity: item.quantity})
                           models.part.update({price: parseInt(part.price), stock: parseInt(part.stock) - parseInt(item.quantity)},{where: {id: partId}})
                        }
                            })
                            })
                        })
                    })
                }
            })
                   
                }                
       
    }else {
        res.json({
            error: "Please Provide full details"
        })
    } 
    })

}
    })
}

exports.request_withdrawal = (req, res, next) => {
        jwt.verify(req.query.token, 'eLFQivL/gdEiW0iuZY1YgdYfF1U=@!2', (err, data) => {
    if(err){
    res.status(400).json({
    error: "Please ensure you've provided a valid token"
    })
    }
    
    else {
    models.user.findOne({where: {id: data.user.id}}).then(user => {
        if(user){
if(req.body.points && req.body.bank_name && req.body.acct_no && req.body.name){
        models.setting.findAll().then(settings => {
            if(req.user.bal > parseInt(req.body.points) || user.bal == req.body.points){
          
        models.withdraw.create({
            amount: parseInt(req.body.points) * settings[0].points,
            points: req.body.points,
            bank: bank_name,
            account_name: req.body.name,
            account_no: req.body.acct_no,
            user_id: user.id
        }).then(wit => {
            models.user.update({bal: req.user.bal - parseInt(req.body.points)}, {where: {id: user.id}})
            res.json({
                message: "Withdrawal request recieved and is being processed",
                amount: "NGN " + parseInt(req.body.points) * settings[0].points
            })
        })  
            }else {
                res.json({
                    error: "Insufficient Points"
                })
            }
            
        })
    }else {
        res.json({
            error: "Please Provide Full Details"
        })
    }
    
    
        }else {
            res.status(400).json({
                error: "Invalid Token Provided"
            })
        }
    })
    }
    })
}
exports.get_shipping = (req, res, next) => {
        if(req.body.lat && req.body.long){
        var lat_main = 9.0710391
        var long_main = 7.437945300000001
        models.amountperkm.findOne().then(amount => {
            var amt = parseInt(amount.price)
            var dist = geodist({lat: lat_main, lon: long_main}, {lat: req.body.lat, lon: req.body.long}, {exact: true, unit: 'km'})
            if(dist){
                res.json({
                    amountperkm: amt,
                    dist,
                    distance: dist + " KM",
                    cost: dist * amnt,
                    message: "Shipping Cost Calculated"
                })
            }else {
                res.json({
                    error: "There was an error with that request"
                })
            }
        })
    }else {
        res.json({
            error: "Please Provide a valid address"
        })
    }
}
exports.forgot_password = (req, res, next) => {
    if(req.body.email){
        models.user.findOne({where: {email: req.body.email}}).then(user => {
            if(user){
                var token = cryptoRandomString({length: 6, type: 'numeric'})
                models.user.update({
                    token
                }, {where: {id: user.id}}).then(rows => {

                    res.json({
                        message: "Check your email for a password reset token"
                    })
                       sendMail(user.email, "Password", `Hi ${user.firstName},You have requested to change your password on GAPAAUTOPARTS your one time token is ${token}`)
                    sendSMS(user.phone, `Hi ${user.firstName},You have requested to change your password on GAPAAUTOPARTS your one time token is ${token}`)
                        			
                })
            }else {
                res.status(401).json({
                    error: "No account was found associated to this email"
                })
            }
        })
    }else {
        
    }
}
exports.reset_password = (req, res, next) => {
    if(req.body.pass && req.body.pass2 && req.body.token){
        if(req.body.pass != req.body.pass2){
         res.status(400).json({
             error: "Password do not match"
         })   
        }
        else {
                   let errors = {}
                   return validateChange(errors, req).then(errors => {
                        		if(!isEmpty(errors)){
                        			res.status(400).json(errors)
                        		}
                        		
                        		
                        		else{
                        		    
                        		      models.user.update({password: bcrypt.hashSync(req.body.pass, bcrypt.genSaltSync(10))}, {where: {token: req.body.token}}).then(rows => {
                        		      models.user.update({token: "******"}, {where: {token: req.body.token}})
                        		          res.json({
                        		              message: "Password Reset succesfully"
                        		          })
                        		      })
                        		    
                        		}
                        		
                        		
                        		})
        }
    }
    else {
      res.json({
          error: "Please Provide Password reset token, New password and password confirmation"
      })  
    }
}

exports.get_customers = (req, res, next) => {
    models.customer.findAll({order: [["createdAt", "DESC"]], include: ["purchases"]}).then(customers => {
        res.json({
            customers
        })
    })
}

exports.get_users = (req, res, next) => {
    models.user.findAll({attributes: {exclude:['password']}, include: ['orders'], order: [["createdAt", "DESC"]]}).then(users => {
        res.json({
            users
        })
    })
}

exports.get_my_customers = (req, res, next) => {
        jwt.verify(req.query.token, 'eLFQivL/gdEiW0iuZY1YgdYfF1U=@!2', (err, data) => {
    if(err){
    res.status(400).json({
    error: "Please ensure you've provided a valid token"
    })
    }
    
    else {
    models.purchase.findAll({where: {retailer_id: data.user.id}, include: [{association: "customer", as: "customer", include: [{association: "purchases", include: ["items"]}]}], order: [["createdAt", "DESC"]]}).then(purchases => {
        res.json({
            purchases
        })
    })
    }
    })
}
exports.get_my_purchases = (req, res, next) => {
        jwt.verify(req.query.token, 'eLFQivL/gdEiW0iuZY1YgdYfF1U=@!2', (err, data) => {
    if(err){
    res.status(400).json({
    error: "Please ensure you've provided a valid token"
    })
    }
    
    else {
    models.purchase.findAll({where: {retailer_id: data.user.id}, include: ["items", "customer"], order: [["createdAt", "DESC"]]}).then(purchases => {
        res.json({
            purchases
        })
    })
    }
    })
}
exports.get_offline_purchases = (req, res, next) => {
    models.purchase.findAll({include: ["items", "retailer"], order: [["createdAt", "DESC"]]}).then(purchases => {
        res.json({
            purchases
        })
    })
}
exports.get_online_purchases = (req, res, next) => {
    models.order.findAll({include: ["items", {association: "user", as: "user", attributes: { exclude: ['password']} }], order: [["createdAt", "DESC"]]}).then(orders => {
        res.json({
            orders
        })
    })
}
exports.get_custs = (req, res, next) => {
    models.customer.findAll().then(customers => {
        res.json({
            customers
        })
    })
}
exports.get_purchase = (req, res, next) => {
    if(req.body.purchase_id){
        models.purchase.findOne({where: {id: req.body.purchase_id}, include: ["retailer", {association: "items", as: "items", include: ["part"]}, "customer"]}).then(purchase => {
            if(purchase){
                res.json({
                    purchase
                })
            }else {
                res.status(404).json({
                    error: "Invalid ID Provided"
                })
            }
        })       
    }else {
        res.status(400).json({
            error: "No Purchase ID Provided"
        })
    }
}

exports.get_retailers = (req, res, next) => {
    models.admin.findAll({where: {role: "retail"}, order: [['createdAt', 'DESC']],attributes: {exclude: ['password']}}).then(retailers => {
        res.json({
            retailers
        })
    })
}
exports.create_retailer = (req, res, next) => {
         if(!req.body.email || !req.body.password || !req.body.name || !req.body.phone || !req.body.address){
     res.json({error: "Incomplete details provided"})   
    }
else{
                 var nums = req.body.phone.split("")
                                 console.log(nums)
                                if(nums[0] == "+"){
                                if(nums[0] == "+" && nums[1] == "2" && nums[2] == "3" ){
                                    var phn = req.body.phone
                                }
                                else {
                                    
                                    var phn = req.body.phone
                                    
                                }
                                
                                }else if(nums[0] == "0") {
                                    nums.shift()
                                
                                nums.unshift("+","2","3","4")
                                var phn = nums.join('')
                                     console.log(phn)
                                }
   
                 
			const hash = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(10));
			var token = cryptoRandomString({length: 6, type: 'numeric'})
			                    
			                            var data = {
                    			    	name: req.body.name,
                    			        phone: phn,
                    			        role: "retail",
                        				email: req.body.email,
                        				password: req.body.password,
                        				img_url: "/img/profile.png",
                        				address: req.body.address,
                    			        }
                                let errors = {};
                        	return validateUser(errors, req).then(errors => {
                        		if(!isEmpty(errors)){
                        			res.status(400).json(errors)
                        		}
                        		
                        		
                        		else{
                        		  
                        			      return models.admin.create(data).then(user => {
                        		            	if(user){
                        		            	   console.log(user);
                                                     	res.json({
                                                     	    message: "Account created successfully, Please verify your account",
                                                     	    id: user.id
                                                     	})
                                                        sendMail(user.email, "Account Creation", `Hi ${user.name}, You have successfully Your Registration On Gapaautoparts as a Retailer is succesful. Your Account password is <b>${user.password}</b>, Username: <b>${user.email}</b>`)
                                                        sendSMS(user.phone, `Hi ${user.name}, You have successfully Your Registration On Gapaautoparts as a Retailer is succesful. Your Account password is ${user.password}, Username: ${user.email}`)
                        			
                        			}
                        
                        			})
                        			.catch(err => {
                        
                        				if(err){
                        					console.log(err);
                        					res.status(500).json({error: "there was an error", err})
                        				}
                        
                        			})	  
                        		}
                        		
                        		
                        		})



		
		}
			


}

exports.get_retailer = (req, res, next) => {
    if(req.query.id){
        models.admin.findOne({where: {role: 'retail', id: req.query.id}, attributes: {exclude: ['password']}}).then(user => {
            models.purchase.findAll({where: {retailer_id: user.id}, include: ["items", "customer"], order: [["createdAt", "DESC"]]}).then(purchases => {
                 var months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
                var data1 = []
                var data2 = []
                months.forEach((month, id) => {
                    var amnt = 0
                    var length = purchases.filter(purchase => moment(purchase.createdAt).month() + 1 == month).length
                    purchases.forEach(p => {
                     if(moment(p.createdAt).month() + 1 == month){
                         amnt += Math.floor(p.amount/1000)
                     }  
                    })
                    data1.push(length)
                    data2.push(amnt)
                })
                var year = 0
                var month = 0
                var day = 0
                var total = 0
                purchases.forEach(pr => {
                if(moment(pr.createdAt).month() == moment().month()){
                         month += pr.amount
                     }
                                if(moment(pr.createdAt).year() == moment().year() ){
                         year += pr.amount
                     }
                if(moment(pr.createdAt).date() == moment().date()){
                         day += pr.amount
                         total += 1
                     }
                })
                
                res.json({
                    retailer: {
                        user,
                        purchases,
                     series: [
                 {
                     name: "Sales",
                     data: data1
                 }
                ],
                series2: [
                 {
                     name: "Revenue",
                     data: data2
                 }
                ],
                year,
                month,
                day
                    }
                })
            })
        })
    }else {
        res.status(400).json({
            error: 'Please Provide Retailer ID'
        })
    }
}

exports.update_profile  = (req, res, next) => {
    jwt.verify(req.query.token, 'eLFQivL/gdEiW0iuZY1YgdYfF1U=@!2', (err, data) => {
    if(err){
    res.status(400).json({
    error: "Please ensure you've provided a valid token"
    })
    }
    
    else {
    if(req.body.address && req.body.name){
        
    }else {
        res.status(400).json({
            error: 'Please Provide Address and Name To Be Updated'
        })
    }
    }
    })
}
    function verif() { 
    jwt.verify(req.token, 'eLFQivL/gdEiW0iuZY1YgdYfF1U=@!2', (err, data) => {
    if(err){
    res.status(400).json({
    error: "Please ensure you've provided a valid token"
    })
    }
    
    else {
    models.maintenance_cat.findAll().then(cats => {
    	res.json({
        	categories: cats
        })
    })
    }
    })
}