const cryptoRandomString = require('crypto-random-string');
var bcrypt = require("bcrypt")
var jwt = require('jsonwebtoken')
const passport = require("passport")
const myPassport = require('../passport_setup')(passport)
var models = require("../models")
var Sequelize = require("sequelize")
const {isEmpty} = require('lodash')
const nodemailer = require("nodemailer")
const {validateUser, validateAdmin} = require("../validators/signup");
const {validateLogin, validateLoginInvestor} = require("../validators/login")
const multer = require('multer');
const path = require('path');
const fs = require('fs'); 
const formidable = require('formidable');
var moment = require("moment")
const cron =  require("node-cron")
var {mail} = require("./mail")

var cur = "&#8358;"
const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './uploads/');
    },

    // By default, multer removes file extensions so let's add them back
    filename: function(req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    }
});

        const imageFilter = function(req, file, cb) {
    // Accept images only
    if (!file.originalname.match(/\.(jpg|JPG|jpeg|JPEG|png|PNG|gif|GIF)$/)) {
        req.fileValidationError = 'Only image files are allowed!';
        return cb(new Error('Only image files are allowed!'), false);
    }
    cb(null, true);
    };
    // 'profile_pic' is the name of our file input field in the HTML form
    let upload = multer({ storage: storage, fileFilter: imageFilter, limits: { fileSize: 2 * 1000 * 1000 }}).single('profile_pic'); 
    
    
const sendMail = (email, subject, page) => {
const transporter = nodemailer.createTransport({
        sendmail: true,
    newline: 'unix',
    path: '/usr/sbin/sendmail'
}, {
  from: "No Reply <no-reply@gapaautoparts.com>",
  to: email,
  subject: subject,
});
transporter.sendMail({html: page}, (error, info) => {
     if (error) {
	console.log(error);
  } else {
    console.log('Email sent: ' + info.response);
  }
})
}


exports.show_index = function(req, res, next){
models.brand.findAll().then(brands => {
models.cat.findAll().then(categories => {

models.part.findAll().then(parts => {
    models.car.findAll().then(cars => {
    models.top.findAll({include: ["part"]}).then(top => {
        models.featured.findAll({where: {category: "tools-&-garage"},include: ["part"], limit: 7}).then(featured_tools => {
            models.featured.findAll({where: {category: "engine-&-drivetrain"},include: ["part"], limit: 7}).then(featured_engine => {
                models.featured.findAll({where: {category: "brakes,-suspension-&-steering"},include: ["part"], limit: 7}).then(featured_brakes => {
    res.render("index", {title: "HOME | GAPANAIJA", user: req.user, brands, parts, featured_tools, featured_engine, featured_brakes, top, cur, moment, cars, categories}) 
            
        })
            })
            })
    })
        
    })
})
     
})
})   

}


exports.login =  (req, res, next) => {
    if(!req.body.email || !req.body.password){
        res.json({error: "Incomplete Login Details"})
    }
    else{
        models.admin.findOne({where: {email: req.body.email}}).then(user => {
            if(user){
                		var errors = {}
	return validateLogin(errors, req).then(errors => {
		if(!isEmpty(errors)){
			res.json(errors)
		}else{
		    if(user.role !== "super" && user.role !== "editor" && user.role != "reviewer"){
		        return res.json({
		            error: "You do not have access to Login"
		        })
		    }
                             	passport.authenticate('local', function(err, user, info) {
                                if (err) { return res.json({error: "there was an error authenticating"})}
                                if (!user) { return res.json({error: "there was an error authenticating"}) }
                                req.logIn(user, function(err) {
                                res.json({message: "Authentication successful", path: req.query.ref ? req.query.ref : "/dashboard"})
                                });
                              })(req, res, next)
		}
                            	})
	
	
            }
            else {
                res.json({
                    error: "incorrect login details"
                })
            }
            
        })
    }
}
exports.show_login = (req, res, next) => {
     if(!req.query.ref){
        res.render("login", {title: "LOGIN | GAPAADMIN", user: req.user, cur, ref: null})
        }else {
            res.render("login", {title: "LOGIN | GAPAADMIN", user: req.user, cur, ref: req.query.ref})
        }

}

exports.show_dashboard = (req, res, next) => {
    models.cat.findAll().then(cats => {
        models.part.findAll({limit: 5, order: [["createdAt", "DESC"]]}).then(parts => {
            models.setting.findAll().then(settings => {
                
    res.render("dash", {title: "ADMIN | GAPAADMIN", user: req.user, cats, cur, parts, setting: settings[0]})
            })
        })
        
    })
}

exports.show_users = (req, res, next) => {
    models.user.findAll().then(users => {
        res.render("users", {title: "ADMIN | GAPAADMIN", user: req.user, users, moment})
    })
}

exports.show_user = (req, res, next) => {
    models.user.findOne({where: {id: req.params.id}}).then(user => {
        if(user){
            res.render("user", {title: "ADMIN | GAPAADMIN", user: req.user, usr: user, moment})
        }else {
            res.redirect("/users")
        }
    })
}

exports.show_addProduct = (req, res, next) => {
    models.brand.findAll({include: [{association: "cars", as: "cars", include: ["models"]}]}).then(brands => {
        models.car.findAll({include: [{association: "models", as: "models", order: [["name", "ASC"]]}]}).then(cars => {
            models.cat.findAll().then(cats => {
                models.maker.findAll().then(makers => {
                    res.render("create_product", {title: "ADMIN | GAPAADMIN", user: req.user, moment, cats, cars, brands, cur, makers})
                })
                
            })
        })
    })
}

exports.show_products = (req, res, next) => { 
   
    if(req.query.page){
  let limit = 7
let offset = 0 + (req.query.page - 1) * limit
var x = offset + 1
models.part.findAndCountAll({
        offset: offset,
        limit: limit,
        order: [
            ['createdAt', 'DESC']
        ]
    }).then(parts => {
        if(parts.count - x > 9){
            var final = false
        }
        else{
            var final = true
        }
            res.render("products", {title: "ADMIN | GAPAADMIN", user: req.user, parts: parts.rows, currentPage: req.query.page, final: final, length: parts.count, moment}) 

        
    })
    }
    else{
           let limit = 7
let offset = 0 + (1 - 1) * limit
var x = offset + 1
    models.part.findAndCountAll({
        offset: offset,
        limit: limit,
        order: [
            ['createdAt', 'DESC']
        ]
    }).then(parts => {
        if(parts.count - x > 9){
            var final = false
        }
        else{
            var final = true
        }
          res.render("products", {title: "ADMIN | GAPAADMIN", user: req.user, parts: parts.rows, currentPage: 1, final: final, length: parts.count, moment})

        
        
    })
    
    }
}

exports.create_product = (req, res, next) => {
    
    const form = new formidable.IncomingForm(); 
    form.parse(req, function(err, fields, files){
        var stt = cryptoRandomString({length: 10})
        var __parentDir = "/home/gapaautoparts/public_html/app/public/"
        var oldPath = files.img.path; 
        var newPath = __parentDir + "uploads" + '/'+ stt + files.img.name 
        var rawData = fs.readFileSync(oldPath) 
        
        var oldPath_2 = files.img_2.path; 
        var newPath_2 = __parentDir + "uploads" + '/'+ stt + files.img_2.name 
        var rawData_2 = fs.readFileSync(oldPath_2) 

        var oldPath_3 = files.img_3.path; 
        var newPath_3 = __parentDir + "uploads" + '/'+ stt + files.img_3.name 
        var rawData_3 = fs.readFileSync(oldPath_3) 
      
        fs.writeFile(newPath, rawData, function(err){ 
            if(err) {res.json({error: "there was an error", err})}    
        fs.writeFile(newPath_2, rawData_2, function(err){ 
            if(err) {res.json({error: "there was an error", err})}
        fs.writeFile(newPath_3, rawData_3, function(err){ 
            if(err) {res.json({error: "there was an error", err})}
            
               else{
        models.model.findOne({where: {id: fields.model_id}, include: [{association: "car", as: "car", include: ["brand"]}]}).then(model => {
            if(model){
                        models.part.create({name: fields.title, description: fields.desc, img_url: "/uploads/" + stt + files.img.name, img_url_1: "/uploads/" + stt + files.img_2.name, img_url_2: "/uploads/" + stt + files.img_3.name, price: parseFloat(fields.price), discount: fields.discount, category: fields.cat, model_id: fields.model_id, available: true, stock: fields.quantity, weight_in_kg: fields.weight, maker_id: fields.maker_id, car_id: model.car.id, brand_id: model.car.brand.id, year: model.year, year2: model.year2, product_code: fields.product_code}).then(part => {
            res.json({
                message: "Product Created Successfully",
                path: `/products/${part.id}`
            })
        })
            }else {
                res.json({
                    error: "Invalid MODEL ID provided"
                })
            }
        })

        
               }
        })
        
        })
        
        })
  }) 
}

exports.show_product = (req, res, next) => {
    models.part.findOne({where: {id: req.params.id}, include: [{association: "model", as: "model", include: [{association: "car", as: "car", include:["brand"]}]}, "maker"]}).then(part => {
        if(part){
            models.cat.findOne({where: {slug: part.category}}).then(cat => {
                        models.brand.findAll({include: [{association: "cars", as: "cars", include: ["models"]}]}).then(brands => {
            models.cat.findAll().then(cats => {
                models.maker.findAll().then(makers => {
                    res.render("product", {title: "ADMIN | GAPANAIJA", user: req.user, part, moment, cat, cur, cats, brands, makers})
                })
            })
        })
               
            })
        }else {
            res.redirect("/products")
        }
    })
}

exports.show_makes = (req, res, next) => {
    models.brand.findAll({order: [["createdAt", 'DESC']]}).then(makes => {
        res.render("makes", {title: "ADMIN | GAPAADMIN", user: req.user, makes, cur, moment})
    })
}

exports.show_cars = (req, res, next) => {
        models.car.findAll({order: [["createdAt", 'DESC']], include: ["brand"]}).then(cars => {
            models.brand.findAll().then(brands => {
        res.render("cars", {title: "ADMIN | GAPAADMIN", user: req.user, cars, brands, cur, moment})    
            })
    })
}

exports.show_models = (req, res, next) => {
        models.brand.findAll({order: [["createdAt", 'DESC']], include: ["cars"]}).then(brands => {
                models.model.findAll({include: [{association: "car", as: "car", include: ["brand"]}]}).then(mods => {
                    
        res.render("models", {title: "ADMIN | GAPAADMIN", user: req.user, brands, models: mods, cur, moment})    
        
                })
            })
}

exports.show_cats = (req, res, next) => {
        models.cat.findAll({order: [["createdAt", 'DESC']]}).then(cats => {
        res.render("cats", {title: "ADMIN | GAPAADMIN", user: req.user, cats, cur, moment})
    })
}

exports.show_makers = (req, res, next) => {
        models.maker.findAll({order: [["createdAt", 'DESC']]}).then(makers => {
        res.render("makers", {title: "ADMIN | GAPAADMIN", user: req.user, makers, cur, moment})
    })
}


exports.create_make = (req, res, next) => {
    
            const form = new formidable.IncomingForm(); 
    form.parse(req, function(err, fields, files){
            if(fields.title){
    models.brand.findOne({where: {name: fields.title}}).then(brand => {
        if(brand){
            res.json({
                error: brand.title + " already exists"
            });
        }else {
        var stt = cryptoRandomString({length: 10})
        var __parentDir = "/home/gapaautoparts/public_html/app/public/"
        var oldPath = files.img.path; 
        var newPath = __parentDir + "uploads" + '/'+ stt + files.img.name 
        var rawData = fs.readFileSync(oldPath) 
      
        fs.writeFile(newPath, rawData, function(err){ 
            if(err) {res.json({error: "there was an error", err})}
            
               else{
    models.brand.create({name: fields.title, slug: fields.title.replace(/\s+/g, '-').toLowerCase(), img_url: "/uploads/" + stt + files.img.name}).then(make => {
                res.json({
                    message: "Brand Created"
                })
            })
        
               }
        })
        }
    })
    
    }else {
        res.json({
            error: "Please Provide Brand title"
        })
    }
  })
     
}

exports.create_cat = (req, res, next) => {

            const form = new formidable.IncomingForm(); 
    form.parse(req, function(err, fields, files){
            if(fields.title){
    models.cat.findOne({where: {title: fields.title}}).then(cat => {
        if(cat){
            res.json({
                error: cat.title + " already exists"
            });
        }else {
        var stt = cryptoRandomString({length: 10})
        var __parentDir = "/home/gapaautoparts/public_html/app/public/"
        var oldPath = files.img.path; 
        var newPath = __parentDir + "uploads" + '/'+ stt + files.img.name 
        var rawData = fs.readFileSync(oldPath) 
      
        fs.writeFile(newPath, rawData, function(err){ 
            if(err) {res.json({error: "there was an error", err})}
            
               else{
                  
        models.cat.create({title: fields.title, slug: fields.title.replace(/\s+/g, '-').toLowerCase(), img_url: "/uploads/" + stt + files.img.name}).then(part => {
            res.json({
                message: "Category Created Successfully"
            })
        })
        
               }
        })
        }
    })
    
    }else {
        res.json({
            error: "Please Provide Category title"
        })
    }
  })
     
}

exports.create_maker = (req, res, next) => {

            const form = new formidable.IncomingForm(); 
    form.parse(req, function(err, fields, files){
            if(fields.title){
    models.maker.findOne({where: {name: fields.title}}).then(maker => {
        if(maker){
            res.json({
                error: maker.name + " already exists"
            });
        }else {
        var stt = cryptoRandomString({length: 10})
        var __parentDir = "/home/gapaautoparts/public_html/app/public/"
        var oldPath = files.img.path; 
        var newPath = __parentDir + "uploads" + '/'+ stt + files.img.name 
        var rawData = fs.readFileSync(oldPath) 
      
        fs.writeFile(newPath, rawData, function(err){ 
            if(err) {res.json({error: "there was an error", err})}
            
               else{
                  
        models.maker.create({name: fields.title,  img_url: "/uploads/" + stt + files.img.name}).then(part => {
            res.json({
                message: "Manufacturer Added Successfully"
            })
        })
        
               }
        })
        }
    })
    
    }else {
        res.json({
            error: "Please Provide Manufacturer title"
        })
    }
  })
     
}

exports.create_car = (req, res, next) => {
    var form = new formidable.IncomingForm(); 
    form.parse(req, function(err, fields, files){
            if(fields.title && fields.brand){

        var stt = cryptoRandomString({length: 10})
        var __parentDir = "/home/gapaautoparts/public_html/app/public/"
        var oldPath = files.img.path; 
        var newPath = __parentDir + "uploads" + '/'+ stt + files.img.name 
        var rawData = fs.readFileSync(oldPath) 
      
        fs.writeFile(newPath, rawData, function(err){ 
            if(err) {res.json({error: "there was an error", err})}
            
               else{
                  
        models.car.create({name: fields.title, model: null, brand_id: fields.brand, img_url: "/uploads/" + stt + files.img.name}).then(part => {
            res.json({
                message: "Car Created Successfully"
            })
        })
        
               }
        }) 
  
    
    }else {
        res.json({
            error: "Please Provide Full Details"
        })
    }
  })
        
}


exports.create_model = (req, res, next) => {
    var form = new formidable.IncomingForm(); 
    form.parse(req, function(err, fields, files){
            if(fields.title && fields.car_id && fields.year && fields.year_2){

        var stt = cryptoRandomString({length: 10})
        var __parentDir = "/home/gapaautoparts/public_html/app/public/"
        var oldPath = files.img.path; 
        var newPath = __parentDir + "uploads" + '/'+ stt + files.img.name 
        var rawData = fs.readFileSync(oldPath) 
      
        fs.writeFile(newPath, rawData, function(err){ 
            if(err) {res.json({error: "there was an error", err})}
            
               else{
                  
        models.model.create({name: fields.title, slug: fields.title.replace(/\s+/g, '-').toLowerCase(), year: fields.year,  year_2: fields.year_2, car_id: fields.car_id, img_url: "/uploads/" + stt + files.img.name}).then(part => {
            res.json({
                message: "Model Created Successfully"
            })
        })
        
               }
        }) 
  
    
    }else {
        res.json({
            error: "Please Provide Full Details"
        })
    }
  })
        
}


exports.show_orders = (req, res, next) => {
         if(req.query.page){
    let limit = 10
let offset = 0 + (req.query.page - 1) * limit
var x = offset + 1

models.order.findAndCountAll({
        offset: req.query.page == 1 ? 0 : offset,
        limit: limit,
        order: [['createdAt', 'DESC']],
        include: ["user"]
    }).then(orders => {
        if(orders.count - x > 9){
            var final = false
        }
        else{
            var final = true
        }
        var totalPages = Math.ceil(orders.count / limit);
            res.render("orders", {title: "ADMIN | GAPAADMIN", user: req.user, orders: orders.rows, currentPage: req.query.page, final: final, length: orders.count, moment, cur, totalPages}) 

        
    })
    }
    else{
           let limit = 10
let offset = 0 + (1 - 1) * limit
var x = offset + 1
    models.order.findAndCountAll({ limit: limit, order: [['createdAt', 'DESC']], include: ["user"]
        }).then(orders => {
        if(orders.count - x > 9){
            var final = false
        }
        else{
            var final = true
        }
        var totalPages = Math.ceil(orders.count / limit);
          res.render("orders", {title: "ADMIN | GAPAADMIN", user: req.user, orders: orders.rows, currentPage: 1, final: final, length: orders.count, moment, cur, totalPages})

        
        
    })
    
    }
}

exports.show_order = (req, res, next) => {
    models.order.findOne({where: {id: req.params.id}, include: ["user",{association: "items", as: "items", include: ["part"]}]}).then(order => {
        if(order){
            if(order.delivery_id){
            
            res.render("order", {title: "ADMIN | GAPAADMIN", user: req.user, order, cur, moment})
            }else {
                models.delivery_men.findAll().then(men => {
                    
                res.render("order", {title: "ADMIN | GAPAADMIN", user: req.user, order, cur, moment, men})
                
                })
            }
        }else {
            res.redirect("/orders")
        }
    })
}

exports.search_products = (req, res, next) => {
    if(req.body.text){
        if(req.body.cat){
            var query = {
                name: Sequelize.where(Sequelize.fn('LOWER', Sequelize.col('name')), 'LIKE', '%' + req.body.text + '%'),
                category: req.body.cat
            }
        }else {
             var query = {
                name: Sequelize.where(Sequelize.fn('LOWER', Sequelize.col('name')), 'LIKE', '%' + req.body.text + '%')
            }
        }
        models.part.findAll({
            limit: 20,
            where: query
        }).then(names => {
            if(names){
               res.json({
                data: names
            }) 
            }else {
        res.json({
            error: "No data found"
        })
            }
        })
    }else {
        res.json({
            error: "Please Provide an input"
        })
    }
}

exports.show_catprods = (req, res, next) => { 
         if(req.query.page){
            
    let limit = 7
let offset = 0 + (req.query.page - 1) * limit
var x = offset + 1

             
models.part.findAndCountAll({
        offset: offset,
        limit: limit,
        order: [
            ['createdAt', 'DESC']
        ],
        where: {
            category: req.params.cat
        }
    }).then(parts => {
        if(parts.count - x > 9){
            var final = false
        }
        else{
            var final = true
        }
            res.render("cat_products", {title: "ADMIN | GAPAADMIN", user: req.user, parts: parts.rows, currentPage: req.query.page, final: final, length: parts.count, moment}) 

        
    })
    }
    else{
           let limit = 7
let offset = 0 + (1 - 1) * limit
var x = offset + 1
 
    models.part.findAndCountAll({
        offset: offset,
        limit: limit,
        order: [
            ['createdAt', 'DESC']
        ],
        where: {
            category: req.params.cat
        }
    }).then(parts => {
        if(parts.count - x > 9){
            var final = false
        }
        else{
            var final = true
        }
          res.render("cat_products", {title: "ADMIN | GAPAADMIN", user: req.user, parts: parts.rows, currentPage: 1, final: final, length: parts.count, moment})

        
        
    })
    
    }
}

exports.edit_cat = (req, res, next) => {

            const form = new formidable.IncomingForm(); 
    form.parse(req, function(err, fields, files){
            if(fields.title && fields.id){
    models.cat.findOne({where: {title: fields.title}}).then(cat => {

        var stt = cryptoRandomString({length: 10})
        var __parentDir = "/home/gapaautoparts/public_html/app/public/"
        var oldPath = files.img.path; 
        var newPath = __parentDir + "uploads" + '/'+ stt + files.img.name 
        var rawData = fs.readFileSync(oldPath) 
      
        fs.writeFile(newPath, rawData, function(err){ 
            if(err) {res.json({error: "there was an error", err})}
            
               else{
        models.cat.findOne({where: {id: fields.id}}).then(cat => {
             models.part.update({
                 category: fields.title.replace(/\s+/g, '-').toLowerCase()
             },{
                where: {
                category: cat.slug
            }
        }).then(rows => {
             models.cat.update({title: fields.title, slug: fields.title.replace(/\s+/g, '-').toLowerCase(), img_url: "/uploads/" + stt + files.img.name}, {where: {id: fields.id}}).then(rows => {
            res.json({
                message: "Category Updated Successfully"
            })
        })
        })   
        })
             
       
        
               }
        })
    })
    
    }else {
        res.json({
            error: "Please Provide Category title and Category ID"
        })
    }
  })
}

exports.edit_cat_2 = (req, res, next) => {
    if(req.body.id && req.body.title){
         models.cat.findOne({where: {id: req.body.id}}).then(cat => {
             models.part.update({
                 category: req.body.title.replace(/\s+/g, '-').toLowerCase()
             },{
                where: {
                category: cat.slug
            }
        }).then(rows => {
             models.cat.update({title: req.body.title, slug: req.body.title.replace(/\s+/g, '-').toLowerCase()}, {where: {id: req.body.id}}).then(rows => {
            res.json({
                message: "Category Updated Successfully"
            })
        })
        })   
        })
    }
    else {
        res.json({
            error: "Please Provide Category ID and Title"
        })
    }
}

exports.edit_brand = (req, res, next) => {

            const form = new formidable.IncomingForm(); 
    form.parse(req, function(err, fields, files){
            if(fields.title && fields.id){

        var stt = cryptoRandomString({length: 10})
        var __parentDir = "/home/gapaautoparts/public_html/app/public/"
        var oldPath = files.img.path; 
        var newPath = __parentDir + "uploads" + '/'+ stt + files.img.name 
        var rawData = fs.readFileSync(oldPath) 
      
        fs.writeFile(newPath, rawData, function(err){ 
            if(err) {res.json({error: "there was an error", err})}
            
               else{
        models.brand.findOne({where: {
            id: fields.id
        }}).then(brand => {
            if(brand){
             
                     models.brand.update({
                        name: fields.title,
                        slug: fields.title.replace(/\s+/g, '-').toLowerCase(),
                        img_url: "/uploads/" + stt + files.img.name
                        }, {
                            where: {
                                id: fields.id
                            }
                        }).then(rows => {
                            if(rows){
                                res.json({
                                    message: "update successful"
                                })
                            }else {
                                res.json({
                                    error: "There was an error updating record"
                                })
                            }
                        })
            }else {
                res.json({
                    error: "Cannot Update a non existent brand"
                })
            }
        })
       
        
               }
        })
    }else {
        res.json({
            error: "Please Provide Brand title and Brand ID"
        })
    }
  })
}
exports.edit_brand_2 = (req, res, next) => {
    if(req.body.id && req.body.title){
        models.brand.findOne({where: {
            id: req.body.id
        }}).then(brand => {
            if(brand){
                     models.brand.update({
                        name: req.body.title,
                        slug: fields.title.replace(/\s+/g, '-').toLowerCase()
                        }, {
                            where: {
                                id: req.body.id
                            }
                        }).then(rows => {
                            if(rows){
                                res.json({
                                    message: "update successful"
                                })
                            }else {
                                res.json({
                                    error: "There was an error updating record"
                                })
                            }
                        })
            }else {
                res.json({
                    error: "Cannot Update a non existent brand"
                })
            }
        })
       
    }else {
        res.json({
            error: "Please Provide Brand ID and Brand Title"
        })
    }
}

exports.edit_car = (req, res, next) => {
      var form = new formidable.IncomingForm(); 
    form.parse(req, function(err, fields, files){
            if(fields.id && fields.title && fields.brand){
        var stt = cryptoRandomString({length: 10})
        var __parentDir = "/home/gapaautoparts/public_html/app/public/"
        var oldPath = files.img.path; 
        var newPath = __parentDir + "uploads" + '/'+ stt + files.img.name 
        var rawData = fs.readFileSync(oldPath) 
      
        fs.writeFile(newPath, rawData, function(err){ 
            if(err) {res.json({error: "there was an error", err})}
            
               else{
                  
        models.car.update({name: fields.title, brand_id: fields.brand, img_url: "/uploads/" + stt + files.img.name}, {where: {id: fields.id}}).then(part => {
            res.json({
                message: "Car Updated Successfully"
            })
        })
        
               }
        }) 

    }else {
        res.json({
            error: "Please Provide Full Details"
        })
    }
  })
 
}

exports.edit_car_2 = (req, res, next) => {
    if(req.body.id && req.body.brand && req.body.title){
          models.car.update({name: req.body.title, brand_id: req.body.brand}, {where: {id: req.body.id}}).then(rows => {
            res.json({
                message: "Car Updated Successfully"
            })
        })
    }else {
        res.json({
            error: "Please Provide full details"
        })
    }
}


exports.edit_model = (req, res, next) => {
      var form = new formidable.IncomingForm(); 
    form.parse(req, function(err, fields, files){
            if(fields.id && fields.title && fields.car_id && fields.year && fields.year_2){
        var stt = cryptoRandomString({length: 10})
        var __parentDir = "/home/gapaautoparts/public_html/app/public/"
        var oldPath = files.img.path; 
        var newPath = __parentDir + "uploads" + '/'+ stt + files.img.name 
        var rawData = fs.readFileSync(oldPath) 
      
        fs.writeFile(newPath, rawData, function(err){ 
            if(err) {res.json({error: "there was an error", err})}
            
               else{
                  
        models.model.update({name: fields.title,slug: fields.title.replace(/\s+/g, '-').toLowerCase(), car_id: fields.car_id, img_url: "/uploads/" + stt + files.img.name, year: fields.year, year_2: fields.year_2}, {where: {id: fields.id}}).then(part => {
            res.json({
                message: "Car Updated Successfully"
            })
        })
        
               }
        }) 

    }else {
        res.json({
            error: "Please Provide Full Details"
        })
    }
  })
 
}

exports.edit_model_2 = (req, res, next) => {
    if(req.body.id && req.body.car_id && req.body.title && req.body.year && req.body.year_2){
          models.model.update({name: req.body.title, car_id: req.body.car_id, year: req.body.year,year_2: req.body.year_2,slug: req.body.title.replace(/\s+/g, '-').toLowerCase()}, {where: {id: req.body.id}}).then(rows => {
            res.json({
                message: "Car Updated Successfully"
            })
        })
    }else {
        res.json({
            error: "Please Provide full details"
        })
    }
}

exports.get_tops = (req, res, next) => {
    models.top.findAll({include: ["part"]}).then(tops => {
        res.render("tops", {title: "ADMIN | GAPAADMIN", user: req.user, moment, cur, tops})
    })
}

exports.get_featured = (req, res, next) => {
    var data = []
    models.cat.findOne({where: {
        slug: "brakes-system"
    }}).then(cat => {
        models.featured.findAll({where: {category: cat.slug}, include: ["part"]}).then(featured_1 => {
            data.push({
                id: cat.id,
                slug: cat.slug,
                title: cat.title,
                parts: featured_1
            })
            models.cat.findOne({where: {
                slug: "engine"
            }}).then(cat => {
                models.featured.findAll({where: {category: cat.slug}, include: ["part"]}).then(featured_2 => {
                    data.push({
                        id: cat.id,
                        slug: cat.slug,
                        title: cat.title,
                        parts: featured_2
                    })
                    models.cat.findOne({where: {
                        slug: "wheels"
                    }}).then(cat => {
                        models.featured.findAll({where: {category: cat.slug}, include: ["part"]}).then(featured_3 => {
                            data.push({
                                id: cat.id,
                                slug: cat.slug,
                                title: cat.title,
                                parts: featured_3
                            })
                            res.render("featured", {title: "ADMIN | GAPAADMIN", user: req.user, data, moment, cur})
                        })
                    })
                })
            })
        })
    })
}

exports.add_top = (req, res, next) => {
    if(req.body.part_id){
        models.top.findOne({where: {part_id: req.body.part_id}, include:["part"]}).then(top => {
            if(!top){
                models.top.create({part_id: req.body.part_id}).then(data => {
                    res.json({
                        message: "Added Successfully"
                    })
                })
            }else {
                res.json({
                    error: "Part/Product already listed as top product"
                })
            }
        })
    }else {
        res.json({
            error: "Product ID not provided"
        })
    }
}

exports.edit_top = (req, res, next) => {
    if(req.body.part_id && req.body.id){
       models.top.findOne({
           where: {
               part_id: req.body.part_id
           }
       }).then(top => {
           if(top){
               res.json({
                   error: "Product/part already in top list"
               })
           }
           else{
            models.top.update({part_id: req.body.part_id}, {where: {id: req.body.id}}).then(rows => {
                res.json({
                    message: "updated successfully"
                })
            })   
           }
       })
    }else {
        res.json({
            error: "Product ID and Top ID not provided"
        })
    }
}

exports.add_featured = (req, res, next) => {
     if(req.body.part_id && req.body.slug){
                models.featured.findOne({where: {category: req.body.slug, part_id: req.body.part_id}}).then(feat => {
                    if(feat){
                        res.json({
                            error: "Product/part already Listed As Featured Under " + feat.category
                        })
                    }else {
                        models.featured.create({part_id: req.body.part_id, category: req.body.slug}).then(rsp => {
                            res.json({
                                message: "Added Successfully"
                            })
                        })
                    }
                })
    }else {
        res.json({
            error: "Product ID and Category ID not provided"
        })
    }
}

exports.remove_featured = (req, res, next) => {
    if(req.body.part_id && req.body.slug){
        models.featured.destroy({where: {part_id: req.body.part_id, category: req.body.slug}}).then(ress => {
            res.json({
                message: "Deleted"
            })
        })
    }else {
        res.json({
            error: "Please Provide Part ID and Category ID"
        })
    }
}

exports.get_settings = (req, res, next) => {
    models.setting.findAll().then(settings => {
        
        if(settings.length > 0){
            var setting = settings[0]
            if(Object.keys(setting).length > 0){
              res.render("contact", {title: "ADMIN | GAPAADMIN", user: req.user, setting: setting, moment})
            }else {
            res.render("contact", {title: "ADMIN | GAPAADMIN", user: req.user, setting: null, moment})
            }
        }else{
            res.render("contact", {title: "ADMIN | GAPAADMIN", user: req.user, setting: null, moment})
        }
    })
}

exports.create_contact = (req, res, next) => {
    if(req.body.address && req.body.hours && req.body.email && req.body.phone && req.body.phone2){
        models.setting.findAll().then(settings => {
            settings.forEach(setting => {
                models.setting.destroy({where: {id: setting.id}})
            })
            models.setting.create({address: req.body.address, hours: req.body.hours, email: req.body.email, phone_1: req.body.phone, phone_2: req.body.phone2}).then(data => {
                res.json({
                    message: "Contact Details Added"
                })
            })
        })
    }else {
        res.json({
            error: "Please Provide Contact Details"
        })
    }
}

exports.edit_contact = (req, res, next) => {
    if(req.body.id && req.body.address && req.body.hours && req.body.email && req.body.phone && req.body.phone2){
            models.setting.update({address: req.body.address, hours: req.body.hours, email: req.body.email, phone_1: req.body.phone, phone_2: req.body.phone2}, {where: {id: req.body.id}}).then(data => {
                res.json({
                    message: "Contact Details Updated"
                })
            })
    }else {
        res.json({
            error: "Please Provide Contact Details"
        })
    }
}

exports.edit_prod_1 = (req, res, next) => {

            const form = new formidable.IncomingForm(); 
    form.parse(req, function(err, fields, files){
            if(fields.id && fields.title && fields.description && fields.price && fields.discount && fields.cat && fields.car_id && fields.stock && fields.weight && fields.maker_id && fields.imgs){
    models.part.findOne({where: {id: fields.id}}).then(prod => {

        var stt = cryptoRandomString({length: 10})
        var __parentDir = "/home/gapaautoparts/public_html/app/public/"
        var oldPath = files.img? files.img.path : ""
        var newPath = files.img? __parentDir + "uploads" + '/'+ stt + files.img.name : ""
        var rawData = files.img? fs.readFileSync(oldPath)  : ""
        
                
        var oldPath_2 = files.img_2? files.img_2.path : ''
        var newPath_2 = files.img_2? __parentDir + "uploads" + '/'+ stt + files.img_2.name : ""
        var rawData_2 = files.img_2?  fs.readFileSync(oldPath_2) : ""

        var oldPath_3 = files.img_3? files.img_3.path : ""
        var newPath_3 = files.img_3? __parentDir + "uploads" + '/'+ stt + files.img_3.name : ""
        var rawData_3 = files.img_3? fs.readFileSync(oldPath_3) : ""
      
        if(fields.imgs == "1_2_3"){
            
        fs.writeFile(newPath, rawData, function(err){ 
            if(err) {res.json({error: "there was an error", err})}
        fs.writeFile(newPath_2, rawData_2, function(err){ 
            if(err) {res.json({error: "there was an error", err})}
        fs.writeFile(newPath_3, rawData_3, function(err){ 
            if(err) {res.json({error: "there was an error", err})}
               else{
                models.model.findOne({where: {id: fields.car_id}, include: [{association: "car", as: "car", include: ["brand"]}]}).then(model => {
            if(model){
        models.part.update({name: fields.title, description: fields.description, img_url: "/uploads/" + stt + files.img.name, img_url_1: "/uploads/" + stt + files.img_2.name, img_url_3: "/uploads/" + stt + files.img_3.name, price: fields.price, discount: fields.discount, category: fields.cat, model_id: fields.car_id, stock: fields.quantity, available: fields.stock === "1"? true : false, weight_in_kg: fields.weight, maker_id: fields.maker_id,car_id: model.car.id, brand_id: model.car.brand.id, year: model.year, year_2: model.year_2},{where: {id: prod.id}}).then(part => {
            res.json({
                message: "Product Updated Successfully"
            })
        })
            }else {
                res.json({
                    error: "Invalid MODEL ID provided"
                })
            }
        })
        
             
       
        
               }
        })
        })
        })
        }else if(fields.imgs == "1_2"){
        fs.writeFile(newPath, rawData, function(err){ 
            if(err) {res.json({error: "there was an error", err})}
        fs.writeFile(newPath_2, rawData_2, function(err){ 
            if(err) {res.json({error: "there was an error", err})}
            
               else{
                models.model.findOne({where: {id: fields.car_id}, include: [{association: "car", as: "car", include: ["brand"]}]}).then(model => {
            if(model){
        models.part.update({name: fields.title, description: fields.description, img_url: "/uploads/" + stt + files.img.name, img_url_1: "/uploads/" + stt + files.img_2.name, price: fields.price, discount: fields.discount, category: fields.cat, model_id: fields.car_id, stock: fields.quantity, available: fields.stock === "1"? true : false, weight_in_kg: fields.weight, maker_id: fields.maker_id,car_id: model.car.id, brand_id: model.car.brand.id, year: model.year, year_2: model.year_2},{where: {id: prod.id}}).then(part => {
            res.json({
                message: "Product Updated Successfully"
            })
        })
            }else {
                res.json({
                    error: "Invalid MODEL ID provided"
                })
            }
        })
        
             
       
        
               }
        })
        
        })
        }
        else if(fields.imgs == "1_3"){
        fs.writeFile(newPath, rawData, function(err){ 
            if(err) {res.json({error: "there was an error", err})}
        fs.writeFile(newPath_3, rawData_3, function(err){ 
            if(err) {res.json({error: "there was an error", err})} 
               else{
                models.model.findOne({where: {id: fields.car_id}, include: [{association: "car", as: "car", include: ["brand"]}]}).then(model => {
            if(model){
        models.part.update({name: fields.title, description: fields.description, img_url: "/uploads/" + stt + files.img.name,img_url_2: "/uploads/" + stt + files.img_3.name, price: fields.price, discount: fields.discount, category: fields.cat, model_id: fields.car_id, stock: fields.quantity, available: fields.stock === "1"? true : false, weight_in_kg: fields.weight, maker_id: fields.maker_id,car_id: model.car.id, brand_id: model.car.brand.id, year: model.year, year_2: model.year_2},{where: {id: prod.id}}).then(part => {
            res.json({
                message: "Product Updated Successfully"
            })
        })
            }else {
                res.json({
                    error: "Invalid MODEL ID provided"
                })
            }
        })
        
             
       
        
               }
        })
        })
        }
        else if(fields.imgs == "2_3"){  
        fs.writeFile(newPath_2, rawData_2, function(err){ 
            if(err) {res.json({error: "there was an error", err})}
        fs.writeFile(newPath_3, rawData_3, function(err){ 
            if(err) {res.json({error: "there was an error", err})}    
               else{
                models.model.findOne({where: {id: fields.car_id}, include: [{association: "car", as: "car", include: ["brand"]}]}).then(model => {
            if(model){
        models.part.update({name: fields.title, description: fields.description, img_url_1: "/uploads/" + stt + files.img_2.name, img_url_2: "/uploads/" + stt + files.img_3.name, price: fields.price, discount: fields.discount, category: fields.cat, model_id: fields.car_id, stock: fields.quantity, available: fields.stock === "1"? true : false, weight_in_kg: fields.weight, maker_id: fields.maker_id,car_id: model.car.id, brand_id: model.car.brand.id, year: model.year, year_2: model.year_2},{where: {id: prod.id}}).then(part => {
            res.json({
                message: "Product Updated Successfully"
            })
        })
            }else {
                res.json({
                    error: "Invalid MODEL ID provided"
                })
            }
        })
        
             
       
        
               }
        })
        })
        }
        else if(fields.imgs == "1"){
        fs.writeFile(newPath, rawData, function(err){ 
            if(err) {res.json({error: "there was an error", err})}
            
               else{
                models.model.findOne({where: {id: fields.car_id}, include: [{association: "car", as: "car", include: ["brand"]}]}).then(model => {
            if(model){
        models.part.update({name: fields.title, description: fields.description, img_url: "/uploads/" + stt + files.img.name, price: fields.price, discount: fields.discount, category: fields.cat, model_id: fields.car_id, stock: fields.quantity, available: fields.stock === "1"? true : false, weight_in_kg: fields.weight, maker_id: fields.maker_id,car_id: model.car.id, brand_id: model.car.brand.id, year: model.year, year_2: model.year_2},{where: {id: prod.id}}).then(part => {
            res.json({
                message: "Product Updated Successfully"
            })
        })
            }else {
                res.json({
                    error: "Invalid MODEL ID provided"
                })
            }
        })
        
             
       
        
               }
        })
        }
        else if(fields.imgs == "2"){
        fs.writeFile(newPath_2, rawData_2, function(err){ 
            if(err) {res.json({error: "there was an error", err})} 
               else{
                models.model.findOne({where: {id: fields.car_id}, include: [{association: "car", as: "car", include: ["brand"]}]}).then(model => {
            if(model){
        models.part.update({name: fields.title, description: fields.description, img_url_1: "/uploads/" + stt + files.img_2.name, price: fields.price, discount: fields.discount, category: fields.cat, model_id: fields.car_id, stock: fields.quantity, available: fields.stock === "1"? true : false, weight_in_kg: fields.weight, maker_id: fields.maker_id,car_id: model.car.id, brand_id: model.car.brand.id, year: model.year, year_2: model.year_2},{where: {id: prod.id}}).then(part => {
            res.json({
                message: "Product Updated Successfully"
            })
        })
            }else {
                res.json({
                    error: "Invalid MODEL ID provided"
                })
            }
        })
        
             
       
        
               }
        })
        }
        else if(fields.imgs == "3"){
        fs.writeFile(newPath_3, rawData_3, function(err){ 
            if(err) {res.json({error: "there was an error", err})}
            
               else{
                models.model.findOne({where: {id: fields.car_id}, include: [{association: "car", as: "car", include: ["brand"]}]}).then(model => {
            if(model){
        models.part.update({name: fields.title, description: fields.description, img_url_2: "/uploads/" + stt + files.img_3.name, price: fields.price, discount: fields.discount, category: fields.cat, model_id: fields.car_id, stock: fields.quantity, available: fields.stock === "1"? true : false, weight_in_kg: fields.weight, maker_id: fields.maker_id,car_id: model.car.id, brand_id: model.car.brand.id, year: model.year, year_2: model.year_2},{where: {id: prod.id}}).then(part => {
            res.json({
                message: "Product Updated Successfully"
            })
        })
            }else {
                res.json({
                    error: "Invalid MODEL ID provided"
                })
            }
        })
        
             
       
        
               }
        })
        }
    })
    
    }else {
        res.json({
            error: "Please Provide Product Details"
        })
    }
  })
}

exports.edit_prod_2 = (req, res, next) => {
    if(req.body.id && req.body.title && req.body.description && req.body.price && req.body.discount && req.body.cat && req.body.car_id && req.body.stock && req.body.weight && req.body.maker_id){
              models.model.findOne({where: {id: req.body.car_id}, include: [{association: "car", as: "car", include: ["brand"]}]}).then(model => {
            if(model){

        models.part.update({name: req.body.title, description: req.body.description, price: parseInt(req.body.price), discount: req.body.discount, category: req.body.cat, model_id: req.body.car_id, stock: req.body.quantity, available: req.body.stock === "1"? true : false, weight_in_kg: req.body.weight, maker_id: req.body.maker_id, car_id: model.car.id, brand_id: model.car.brand.id, year: model.year, year_2: model.year_2, product_code: req.body.product_code},{where: {id: req.body.id}}).then(part => {
            res.json({
                message: "Product Created Successfully"
            })
        })
            }else {
                res.json({
                    error: "Invalid MODEL ID provided"
                })
            }
        })

    }
    else {
        res.json({
            error: "Please Provide Product Details"
        })
    }
}

exports.update_status = (req, res, next) => {
    if(req.body.status && req.body.id){
        models.order.update({status: req.body.status}, {where: {id: req.body.id}}).then(rows => {
            res.json({
                message: "Status Updated Successfully"
            })
        })
    }else {
        res.json({
            error: "Please Provide STATUS and PRODUCT ID"
        })
    }
}

exports.get_tc = (req, res, next) => {
    models.page.findOne({where: {name: "tc"}}).then(page => {
        res.render("tc", {title: "ADMIN | GAPAADMIN", user: req.user, page, moment})
    })
}

exports.get_privacy = (req, res, next) => {
    models.page.findOne({where: {name: "privacy"}}).then(page => {
        res.render("privacy", {title: "ADMIN | GAPAADMIN", user: req.user, page, moment})
    })
}

exports.get_about = (req, res, next) => {
    models.page.findOne({where: {name: "about"}}).then(page => {
        res.render("aboutus", {title: "ADMIN | GAPAADMIN", user: req.user, page, moment})
    })
}


exports.create_tc = (req, res, next) => {
    if(req.body.tc){
        models.page.create({name: "tc", body: req.body.tc}).then(tc => {
            res.json({
                messsage: "Terms and Conditions Added"
            })
        })
    }else {
        error: "Please Provide Terms and Conditions"
    }
}

exports.create_about = (req, res, next) => {
    if(req.body.about){
        models.page.create({name: "about", body: req.body.about}).then(tc => {
            res.json({
                messsage: "About Us Added"
            })
        })
    }else {
        error: "Please Provide About Us"
    }
}

exports.create_privacy = (req, res, next) => {
    if(req.body.privacy){
        models.page.create({name: "privacy", body: req.body.privacy}).then(tc => {
            res.json({
                messsage: "Privacy Policy Added"
            })
        }) 
    }else {
        res.json({
            error: "Please Provide Privacy Policy"
        })
    }
}

exports.edit_tc = (req, res, next) => {
    if(req.body.tc && req.body.id){
        models.page.update({name: "tc", body: req.body.tc}, {where: {id: req.body.id}}).then(tc => {
            res.json({
                messsage: "Terms and Conditions Updated"
            })
        })
    }else {
        error: "Please Provide Terms and Conditions and ID"
    }
}

exports.edit_privacy = (req, res, next) => {
    if(req.body.privacy && req.body.id){
        models.page.update({name: "privacy", body: req.body.privacy}, {where: {id: req.body.id}}).then(tc => {
            res.json({
                messsage: "Privacy Policy Updated"
            })
        }) 
    }else {
        res.json({
            error: "Please Provide Privacy Policy And ID"
        })
    }
}


exports.edit_about = (req, res, next) => {
    if(req.body.about && req.body.id){
        models.page.update({name: "about", body: req.body.about}, {where: {id: req.body.id}}).then(tc => {
            res.json({
                messsage: "About Us Updated"
            })
        }) 
    }else {
        res.json({
            error: "Please Provide About Us And ID"
        })
    }
}

exports.get_withdraws = (req, res, next) => {
    models.withdraw.findAll({order: [["createdAt", 'DESC']], include: ["user"]}).then(withdraws => {
        res.render("withdraws", {title: "ADMIN | GAPAADMIN", user: req.user, withdraws, moment})
    })
}
exports.accept_withdraw = (req, res, next) => {
    if(req.body.id){
        models.withdraw.update({status: "success"}, {where: {id: req.body.id}}).then(rows => {
            res.json({
                message: "Withdrawal Request Confirmed"
            })
        })
    }else {
        res.json({
            error: "Please Provide Withdrawal ID"
        })
    }
}
exports.decline_withdraw = (req, res, next) => {
        if(req.body.id){
        models.withdraw.findOne({where: {id: req.body.id}}).then(wth => {
            models.user.findOne({where: {id: wth.user_id}}).then(user => {
            models.withdraw.update({status: "declined"}, {where: {id: req.body.id}}).then(rows => {
                models.user.update({bal: user.bal + wth.points},{where: {id: wth.user_id}})
            res.json({
                message: "Withdrawal Request Declined"
            })
        })
        })
             
                
            })   
    }else {
        res.json({
            error: "Please Provide Withdrawal ID"
        })
    }
}

exports.add_logistic = (req, res, next) => {
    if(req.body.order_id){
        models.order.findOne({where: {id: req.body.order_id}}).then(order => {
            if(order){
                models.user.findOne({where: {id: order.user_id}}).then(user => {
                    if(user){
                        models.logistic.create({
                            name: user.firstName + " " + user.lastName,
                            phone: user.phone,
                            email: user.email,
                            pickup: "5 O.P. Fingesi Road, Abuja, Nigeria",
                            depart: order.delivery_address,
                            currentadd: order.delivery_address,
                            cost: order.shipping_cost,
                            payment: order.shipping_cost,
                            pending: 1,
                            ongoing: 0,
                            delivered: 0,
                            rejected: 0,
                            delivery_code: cryptoRandomString({length: 8, type: 'alphanumeric'})
                        }).then(logistic => {
                            models.order.update({
                                delivery_id: logistic.id
                            }, {
                                where: {
                                    id: order.id
                                }
                            })
                            res.json({
                                message: "Logistic Added Succesfully"
                            })
                        })
                    }else {
                        res.json({
                            error: "user not found"
                        })
                    }
                })
            }else {
                res.json({
                    error: "Order Not found"
                })
            }
        })
    }else {
        res.json({
            error: "Please Provide Order ID and Delivery Man ID"
        })
    }
}

exports.get_points = (req, res, next) => {
    models.setting.findAll().then(settings => {
        res.render("points", {title: "ADMIN | GAPAADMIN", user: req.user, settings})
    })
}

exports.get_img = (req, res, next) => {
    models.setting.findAll().then(settings => {
        res.render("img", {title: "ADMIN | GAPAADMIN", user: req.user, settings})
    })
}

exports.update_logo = (req, res, next) => {
         const form = new formidable.IncomingForm(); 
    form.parse(req, function(err, fields, files){

        var stt = cryptoRandomString({length: 3})
        var __parentDir = "/home/gapaautoparts/public_html/app/public/"
        var oldPath = files.img.path; 
        var newPath = __parentDir + "uploads" + '/'+ stt + files.img.name 
        var rawData = fs.readFileSync(oldPath) 
      
        fs.writeFile(newPath, rawData, function(err){ 
            if(err) {res.json({error: "there was an error", err})}
            
               else{
                models.setting.findAll().then(settings => {
                    if(settings.length > 0){
                        models.setting.update({logo: "/uploads/" + stt + files.img.name}, {where: {id: settings[0].id}}).then(rows => {
                            res.json({
                                message: "Logo Updated"
                            })
                        })
                    }
                    else {
                    models.setting.create({logo: "/uploads/" + stt + files.img.name}).then(rows => {
                            res.json({
                                message: "Logo Updated"
                            })
                        })
                    }
                })
               }
        })

  })    
}
exports.update_slider_1 = (req, res, next) => {
         const form = new formidable.IncomingForm(); 
    form.parse(req, function(err, fields, files){

        var stt = cryptoRandomString({length: 3})
        var __parentDir = "/home/gapaautoparts/public_html/app/public/"
        var oldPath = files.img.path; 
        var newPath = __parentDir + "uploads" + '/'+ stt + files.img.name 
        var rawData = fs.readFileSync(oldPath) 
      
        fs.writeFile(newPath, rawData, function(err){ 
            if(err) {res.json({error: "there was an error", err})}
            
               else{
                models.setting.findAll().then(settings => {
                    if(settings.length > 0){
                        models.setting.update({slider_1: "/uploads/" + stt + files.img.name}, {where: {id: settings[0].id}}).then(rows => {
                            res.json({
                                message: "Logo Updated"
                            })
                        })
                    }
                    else {
                    models.setting.create({slider_1: "/uploads/" + stt + files.img.name}).then(rows => {
                            res.json({
                                message: "Logo Updated"
                            })
                        })
                    }
                })
               }
        })

  })    
}
exports.update_slider_2 = (req, res, next) => {
         const form = new formidable.IncomingForm(); 
    form.parse(req, function(err, fields, files){

        var stt = cryptoRandomString({length: 3})
        var __parentDir = "/home/gapaautoparts/public_html/app/public/"
        var oldPath = files.img.path; 
        var newPath = __parentDir + "uploads" + '/'+ stt + files.img.name 
        var rawData = fs.readFileSync(oldPath) 
      
        fs.writeFile(newPath, rawData, function(err){ 
            if(err) {res.json({error: "there was an error", err})}
            
               else{
                models.setting.findAll().then(settings => {
                    if(settings.length > 0){
                        models.setting.update({slider_2: "/uploads/" + stt + files.img.name}, {where: {id: settings[0].id}}).then(rows => {
                            res.json({
                                message: "Logo Updated"
                            })
                        })
                    }
                    else {
                    models.setting.create({slider_2: "/uploads/" + stt + files.img.name}).then(rows => {
                            res.json({
                                message: "Logo Updated"
                            })
                        })
                    }
                })
               }
        })

  })    
}

exports.update_slider_3 = (req, res, next) => {
         const form = new formidable.IncomingForm(); 
    form.parse(req, function(err, fields, files){

        var stt = cryptoRandomString({length: 3})
        var __parentDir = "/home/gapaautoparts/public_html/app/public/"
        var oldPath = files.img.path; 
        var newPath = __parentDir + "uploads" + '/'+ stt + files.img.name 
        var rawData = fs.readFileSync(oldPath) 
      
        fs.writeFile(newPath, rawData, function(err){ 
            if(err) {res.json({error: "there was an error", err})}
            
               else{
                models.setting.findAll().then(settings => {
                    if(settings.length > 0){
                        models.setting.update({slider_3: "/uploads/" + stt + files.img.name}, {where: {id: settings[0].id}}).then(rows => {
                            res.json({
                                message: "Logo Updated"
                            })
                        })
                    }
                    else {
                    models.setting.create({slider_3: "/uploads/" + stt + files.img.name}).then(rows => {
                            res.json({
                                message: "Logo Updated"
                            })
                        })
                    }
                })
               }
        })

  })    
}

exports.update_points = (req, res, next) => {
    if(req.body.points){
         models.setting.findAll().then(settings => {
        if(settings.length > 0){
             models.setting.update({points: req.body.points}, {where: {id: settings[0].id}}).then(data => {
                res.json({
                    message: "Points Ratio Updated"
                })
            })
        }else {
             models.setting.create({address:  "", hours: "", email: "", phone_1: "", phone_2: "", points: req.body.points}).then(data => {
                res.json({
                    message: "Points Ratio Updated"
                })
            })
        }
        
    })

    }else {
        res.json({
            error: "Please provide POINTS RATIO OF REFERRAL EARNING"
        })
    }
}

exports.edit_maker = (req, res, next) => {

            const form = new formidable.IncomingForm(); 
    form.parse(req, function(err, fields, files){
            if(fields.title && fields.id){
        var stt = cryptoRandomString({length: 10})
        var __parentDir = "/home/gapaautoparts/public_html/app/public/"
        var oldPath = files.img.path; 
        var newPath = __parentDir + "uploads" + '/'+ stt + files.img.name 
        var rawData = fs.readFileSync(oldPath) 
      
        fs.writeFile(newPath, rawData, function(err){ 
            if(err) {res.json({error: "there was an error", err})}
            
               else{

             models.maker.update({name: fields.title, img_url: "/uploads/" + stt + files.img.name}, {where: {id: fields.id}}).then(rows => {
            res.json({
                message: "Product Manufacturer Updated Successfully"
            })
        })

             
       
        
               }
        })

    
    }else {
        res.json({
            error: "Please Provide Category title and Category ID"
        })
    }
  })
}

exports.edit_maker_2 = (req, res, next) => {
    if(req.body.id && req.body.title){

             models.maker.update({name: req.body.title}, {where: {id: req.body.id}}).then(rows => {
            res.json({
                message: "Category Updated Successfully"
            })
        })
    }
    else {
        res.json({
            error: "Please Provide Maker ID and Title"
        })
    }
}


exports.get_send = (req, res, next) => {
    res.render("email", {title: "ADMIN | GAPAADMIN", user: req.user, moment})
}

exports.send_mail = (req, res, next) => {
    if(req.body.subject && req.body.body && req.body.rec){
        var ml = mail(req.body.subject, req.body.body)
        models.user.findAll().then(users => {
                if(req.body.rec == "0"){
            users.forEach(user => {
                 sendMail(user.email, req.body.subject, ml)
            })       
        }else if(req.body.rec == "1"){
            models.mailing.findAll().then(users => {
              
            users.forEach(user => {
                 sendMail(user.email, req.body.subject, ml)
            })         
            })
        }
        else {
        sendMail(req.body.rec, req.body.subject, ml)
  
        }   res.json({
            message: "Message Sent"
            
        })
            
        })
      
      
    }else {
        res.json({
            error: "Incomplete details provided"
        })
    }
}

exports.delete_brand = (req, res, next) => {
    if(req.body.brand_id){
        models.brand.findOne({where: {
            id: req.body.brand_id
        }}).then(brand => {
            if(brand){
                models.car.findAll({
                    where: {
                        brand_id: brand.id
                    }
                }).then(cars => {
                    cars.forEach(car => {
                        models.model.findAll({where: {car_id: car.id}}).then(mods => {
                            mods.forEach(mod => {
                                
                                models.part.findAll({where: {
                                model_id: mod.id
                                }}).then(parts => {
                                parts.forEach(part => {
                                models.item.findAll({where: {part_id: part.id}}).then(items => {
                                if(items.length > 0){
                                
                                items.forEach(item => {
                                models.order.destroy({where: {id: items[0].order_id}})
                                models.item.destroy({where: {id: item.id}})                                       
                                })

                                }
                                })
                                models.wish.destroy({where: {part_id: part.id}})   
                                  
                                models.part.destroy({where: {id: part.id}})   
                                }) 
                                })
                            
                                models.model.destroy({where: {
                                    id: mod.id
                                }})
                            })
                        })
                        models.car.destroy({where: {id: car.id}})
                    })
                
                });
                models.brand.destroy({where: {id: brand.id}}).then(rows => {
                    res.json({
                        message: "Deleted"
                    })
                })
            }else {
                res.json({
                    error: "Invalid Brand ID Provided"
                })
            }
        })
    }else {
        res.json({
            error: "Please Provide Brand ID"
        })
    }
}
exports.delete_car = (req, res, next) => {
    if(req.body.car_id){
            
            models.car.findOne({
                    where: {
                        id: req.body.car_id
                    }
                }).then(car => {
                    if(car){
                        models.model.findAll({where: {car_id: car.id}}).then(mods => {
                            mods.forEach(mod => {
                                
                                models.part.findAll({where: {
                                model_id: mod.id
                                }}).then(parts => {
                                parts.forEach(part => {
                                models.item.findAll({where: {part_id: part.id}}).then(items => {
                                if(items.length > 0){
                                
                                items.forEach(item => {
                                models.order.destroy({where: {id: items[0].order_id}})
                                models.item.destroy({where: {id: item.id}})                                       
                                })

                                }
                                })
                                models.wish.destroy({where: {part_id: part.id}})   
                                  
                                models.part.destroy({where: {id: part.id}})   
                                }) 
                                })
                            
                                models.model.destroy({where: {
                                    id: mod.id
                                }})
                            })
                        })
                        models.car.destroy({where: {id: car.id}})
                        res.json({
                            message: "Deleted"
                        })
                    }else {
                        res.json({
                            error: "Invalid Car ID"
                        })
                    }
                
                });
    }else {
        res.json({
            error: "Please Provide Car ID"
        })
    }
}
exports.delete_model = (req, res, next) => {
        if(req.body.model_id){
           models.model.findOne({where: {id: req.bod.model_id}}).then(mod => {
                                
                                models.part.findAll({where: {
                                model_id: mod.id
                                }}).then(parts => {
                                parts.forEach(part => {
                                models.item.findAll({where: {part_id: part.id}}).then(items => {
                                if(items.length > 0){
                                
                                items.forEach(item => {
                                models.order.destroy({where: {id: item.order_id}})
                                models.item.destroy({where: {id: item.id}})                                       
                                })

                                }
                                })
                                models.wish.destroy({where: {part_id: part.id}})   
                                  
                                models.part.destroy({where: {id: part.id}})   
                                }) 
                                })
                            
                                models.model.destroy({where: {
                                    id: mod.id
                                }})
                                
                                res.json({
                                    message: "Deleted"
                                })
                        })
    }else {
        res.json({
            error: "Please Provide Model ID"
        })
    }
}
exports.delete_maker = (req, res, next) => {
        if(req.body.maker_id){
        models.maker.findOne({where: {id: req.body.maker_id}}).then(mod => {
            if(mod){
                  models.part.findAll({where: {
                                maker_id: mod.id
                                }}).then(parts => {
                                parts.forEach(part => {
                                models.item.findAll({where: {part_id: part.id}}).then(items => {
                                if(items.length > 0){
                                
                                items.forEach(item => {
                                models.order.destroy({where: {id: item.order_id}})
                                models.item.destroy({where: {id: item.id}})                                       
                                })

                                }
                                })
                                models.wish.destroy({where: {part_id: part.id}})   
                                  
                                models.part.destroy({where: {id: part.id}})   
                                }) 
                                })
                            
                                models.maker.destroy({where: {
                                    id: mod.id
                                }})
                                
                                res.json({
                                    message: "Deleted"
                                })
            }else {
                res.json({
                    error: "Maker ID invalid"
                })
            }
        })
    }else {
        res.json({
            error: "Please Provide Maker ID"
        })
    }
}
exports.delete_prod = (req, res, next) => {
        if(req.body.prod_id){
          models.part.findOne({where: {
                                id: req.body.prod_id
                                }}).then(part => {
                                models.item.findAll({where: {part_id: part.id}}).then(items => {
                                if(items.length > 0){
                               
                                items.forEach(item => {
                                models.order.destroy({where: {id: item.order_id}})
                                models.item.destroy({where: {id: item.id}})                                       
                                })

                                }
                                })
                                models.wish.destroy({where: {part_id: part.id}})   
                                  
                                models.part.destroy({where: {id: part.id}})   
                                res.json({
                                    message: "Deleted"
                                })
                                })

    }else {
        res.json({
            error: "Please Provide Product ID"
        })
    }   
}

exports.toggle_coming = (req, res, next) => {
    if(req.body.status){
    models.setting.findAll().then(settings => {
        if(settings.length > 0){
           models.setting.update({coming_soon: req.body.status === 1 ? "true" : "false"}, {where: {id: settings[0].id}}).then(dtt => {
                  res.json({
                     message: "Updated"
                 })
           })
        }else {
             models.setting.create({address:  "", hours: "", email: "", phone_1: "", phone_2: "", points: 0.5, coming_soon: req.body.status === 1 ? "true" : "false"}).then(dt => {
                 res.json({
                     message: "Updated"
                 })
             })
        }
    })
    }else {
        res.json({
            error: "Please Provide Status"
        })
    }
}

exports.find_by_code = (req, res, next) => {
    if(req.body.text){
        models.part.findAll({
            limit: 50,
            where: {
                product_code: req.body.text
            },
            include: [{association: "model", as: 'model', include: [{association: "car", as: "car", include: ['brand']}]}]
        }).then(names => {
            if(names){
               res.json({
                data: names
            }) 
            }else {
        res.json({
            error: "No data found"
        })
            }
        })
    }else {
        res.json({
            error: "Please Provide an input"
        })
    }
}

exports.get_admins = (req, res, next) => {
  models.admin.findAll().then(admins => {
      res.render("admins", {title: "ADMIN | GAPAADMIN", user: req.user, admins, cur, moment})
  })   
}

exports.createAdmin = (req, res, next) => {
        if (!req.body.email || !req.body.password || !req.body.name || !req.body.role || !req.body.phone) {
        res.json({
            error: "Incomplete details provided"
        })
    } else {

        
        var data = {
            firstName: req.body.name,
            phone: req.body.phone,
            email: req.body.email,
            password: hash,
            img_url: "/img/profile.png",
            role: req.body.role
        }
        let errors = {};
        return validateAdmin(errors, req).then(errors => {
            if (!isEmpty(errors)) {
                res.json(errors)
            } else {

                        

                            return models.admin.create(data).then(user => {
                                if (user) {
                                    console.log(user);

                                    sendMail(user.email, "Email verification", mail('Email Verification',`<p>Hi ${user.firstName}, You have successfully signed up to Gapaautoparts.</p><p>Your verification token is ${user.token}</p>`))
                                    sendSMS(user.phone, `Hi ${user.firstName}, You have successfully signed up to Gapaautoparts. Your verification token is ${user.token}`)
                                    res.json({
                                        message: "User Added Successfully"
                                    })

                                }

                            })
                        

                    .catch(err => {

                        if (err) {
                            console.log(err);
                            res.json({
                                error: "there was an error",
                                err
                            })
                        }

                    })
            }


        })




    }


}
exports.get_searches = function(req, res, next){
    models.search.findAll().then(searches => {
        res.render("searches", {title: "ADMIN | GAPAADMIN", user: req.user, searches, moment})
    })
}