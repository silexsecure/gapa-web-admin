const cryptoRandomString = require('crypto-random-string');
var bcrypt = require("bcrypt")
var jwt = require('jsonwebtoken')
const passport = require("passport")
const myPassport = require('../passport_setup')(passport)
var models = require("../models")
var Sequelize = require("sequelize")
const {isEmpty} = require('lodash')
const nodemailer = require("nodemailer")
const {validateUser, validateInvestor} = require("../validators/signup");
const {validateLogin, validateLoginInvestor} = require("../validators/login")
const {validateChange} = require("../validators/change")
const multer = require('multer');
const path = require('path');
const fs = require('fs'); 
const formidable = require('formidable');
var moment = require("moment")
const cron =  require("node-cron")
var {mail} = require("./mail")
const AfricasTalking = require('africastalking');
const geodist = require("geodist")
// TODO: Initialize Africa's Talking
const africastalking = AfricasTalking({
  apiKey: '795bbe4af0239786441268f6c723950ca10f1408e66a3775d6e8b61d6a86b85a', 
  username: 'century21'
});
async function sendSMS(num, msg) {
    
    try {
  const result = await africastalking.SMS.send({
    to: num, 
    message: msg
  });
  console.log(result);
} catch(ex) {
  console.error(ex);
} 
};
var cur = "&#8358;"
const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './uploads/');
    },

    // By default, multer removes file extensions so let's add them back
    filename: function(req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    }
});

        const imageFilter = function(req, file, cb) {
    // Accept images only
    if (!file.originalname.match(/\.(jpg|JPG|jpeg|JPEG|png|PNG|gif|GIF)$/)) {
        req.fileValidationError = 'Only image files are allowed!';
        return cb(new Error('Only image files are allowed!'), false);
    }
    cb(null, true);
    };
    // 'profile_pic' is the name of our file input field in the HTML form
    let upload = multer({ storage: storage, fileFilter: imageFilter, limits: { fileSize: 2 * 1000 * 1000 }}).single('profile_pic'); 
    
    
const sendMail = (email, subject, page) => {
const transporter = nodemailer.createTransport({sendmail: true}, {
  from: "SUPPORT@GAPANAIJA.com",
  to: email,
  subject: subject,
});
transporter.sendMail({html: page}, (error, info) => {
     if (error) {
	console.log(error);
  } else {
    console.log('Email sent: ' + info.response);
  }
})
}


exports.login = (req, res, next) => {
const emphone = req.body.email_or_phone
const password = req.body.password

if(!emphone || !password) {

	res.status(400).json({
    error: "Incomplete Details provided"
    })

}

else {
	
	models.user.findOne({where: {email: req.body.email_or_phone}}).then(user => {
    	if(user){
    	if(!user.emailVerified){
    	    return res.status(403).json({
    	        error: "Account Not Verified"
    	    })
    	}
        const user_data = {
        id: user.id,
        first_name: user.firstName,
        last_name: user.lastName,
        profile_image: "https://gapaautoparts.com" + user.img_url,
        phone_number: user.phone,
        state: user.state,
        address: user.address,
        email: user.email,
        bal: user.bal,
        referral_code: user.referral_code
        }
        const is_pass_valid = bcrypt.compareSync(req.body.password, user.password)
        if(is_pass_valid){
        const token = jwt.sign({user: user_data}, "eLFQivL/gdEiW0iuZY1YgdYfF1U=@!")
        res.json({
        message: "Signed in Successfully",
        token: token,
        user_data: user_data
        })
        }
        else {
        res.status(401).json({
        error: "Incorrect Login Details"
        })
        }
        }
    	else {
     var nums = req.body.email_or_phone.split("")
                                 console.log(nums)
                                if(nums[0] == "+"){
                                if(nums[0] == "+" && nums[1] == "2" && nums[2] == "3" ){
                                    var phn = req.body.email_or_phone
                                }
                                else {
                                    
                                    var phn = req.body.email_or_phone
                                    
                                }
                                
                                }else if(nums[0] == "0") {
                                    nums.shift()
                                
                                nums.unshift("+","2","3","4")
                                var phn = nums.join('')
                                     console.log(phn)
                                }
    	models.user.findOne({where: {phone: phn}}).then(user => {
    	    if(user){
    	          	if(!user.emailVerified){
    	    return res.status(403).json({
    	        error: "Account Not Verified"
    	    })
    	}
        const user_data = {
        id: user.id,
        first_name: user.firstName,
        last_name: user.lastName,
        profile_image: "https://gapaautoparts.com" + user.img_url,
        phone_number: user.phone,
        state: user.state,
        address: user.address,
        email: user.email,
        bal: user.bal,
        referral_code: user.referral_code
        }
        const is_pass_valid = bcrypt.compareSync(req.body.password, user.password)
        if(is_pass_valid){
        const token = jwt.sign({user: user_data}, "eLFQivL/gdEiW0iuZY1YgdYfF1U=@!")
        res.json({
        message: "Signed in Successfully",
        token: token,
        user_data: user_data
        })
        }
        else {
        res.status(401).json({
        error: "Incorrect Login Details"
        })
        }  
    	    }else {
        res.status(401).json({
        	error: "User Account Not found"
        })
    	        
    	    }
    	})
        }
    })
	
}
}

exports.register = (req, res, next) => {
     if(!req.body.email || !req.body.password || !req.body.fname || !req.body.lname || !req.body.phone || !req.body.address){
     res.json({error: "Incomplete details provided"})   
    }
else{
                 var nums = req.body.phone.split("")
                                 console.log(nums)
                                if(nums[0] == "+"){
                                if(nums[0] == "+" && nums[1] == "2" && nums[2] == "3" ){
                                    var phn = req.body.phone
                                }
                                else {
                                    
                                    var phn = req.body.phone
                                    
                                }
                                
                                }else if(nums[0] == "0") {
                                    nums.shift()
                                
                                nums.unshift("+","2","3","4")
                                var phn = nums.join('')
                                     console.log(phn)
                                }
                                            if(req.body.ref){
                models.user.findOne({where: {referral_code: req.body.ref}}).then(reff => {
                    if(reff){
                        models.user.update({
                            bal: reff.bal + 2
                        }, {where: {
                            id: reff.id
                        }})
                    }
                })
            }
                 
			const hash = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(10));
			var token = cryptoRandomString({length: 6, type: 'numeric'})
			                    
			                            var data = {
                    			    	firstName: req.body.fname,
                    			    	lastName: req.body.lname,
                    			        phone: phn,
                        				email: req.body.email,
                        				password: hash,
                        				token: token,
                        				img_url: "/img/profile.png",
                        				address: req.body.address,
                        				referral_code: cryptoRandomString({length: 4, type: 'distinguishable'}) + "-" + cryptoRandomString({length: 4, type: 'numeric'})
                    			        }
                                let errors = {};
                        	return validateUser(errors, req).then(errors => {
                        		if(!isEmpty(errors)){
                        			res.status(400).json(errors)
                        		}
                        		
                        		
                        		else{
                        		  
                        			      return models.user.create(data).then(user => {
                        		            	if(user){
                        		            	   console.log(user);
                                                     	res.json({
                                                     	    message: "Account created successfully, Please verify your account"
                                                     	})
                                                        sendMail(user.email, "Email verification", `your verification token is ${user.token}`)
                                                        sendSMS(user.phone, `Hi ${user.firstName}, You have successfully signed up to Gapaautoparts. Your verification token is ${user.token}`)
                        			
                        			}
                        
                        			})
                        			.catch(err => {
                        
                        				if(err){
                        					console.log(err);
                        					res.status(500).json({error: "there was an error", err})
                        				}
                        
                        			})	  
                        		}
                        		
                        		
                        		})



		
		}
			



}

exports.verify = (req, res, next) => {
        if(!req.body.token){
       res.json({error: "Incomplete details provided"})
   }
   else{
       models.user.findOne({where: {token: req.body.token}}).then(usr => {
           if(!usr){
               res.status(400).json({error: "Invalid token provided"})
           }else if(usr){
               models.user.update({emailVerified: true, token: "******"}, {where: {id: usr.id}}).then((rows) => {
                   if(rows){
                       res.json({message: "email verification successful"})
                   }
                   else{
                       res.status(400).json({error: "an error occurred please try again"})
                   }
               })
           }
       })
       
       
       
   }
}

exports.get_top = (req, res, next) => {
    models.top.findAll({include: ["part"]}).then(tops => {
        var top_products = []
        tops.forEach(top => {
            top_products.push(top.part)
        })
        res.json(top_products)
    })
}

exports.get_featured = (req, res, next) => {
        models.featured.findAll({include: ["part"]}).then(tops => {
        var featured_products = []
        tops.forEach(top => {
            featured_products.push(top.part)
        })
        res.json(featured_products)
    })
}


exports.get_cats = (req, res, next) => {
    models.cat.findAll().then(cats => {
        var categories = []
        cats.forEach(cat => {
            categories.push({
                id: cat.id,
                title: cat.title,
                img_url: cat.img_url,
                createdAt: cat.createdAt
            })
        })
        res.json(categories)
    })
}

exports.get_brands = (req, res, next) => {
    models.brand.findAll().then(brands => {
        res.json(brands)
    })
}

exports.get_cars = (req, res, next) => {
   if(req.query.brand_id){
       models.brand.findOne({where: {id: req.query.brand_id}}).then(brand => {
           if(brand){
               
       models.car.findAll({where: {brand_id: req.query.brand_id}}).then(cars => {
           res.json({
               brand,
               cars
           })
       })
           }else {
               res.status(404).json({
                   error: "Invalid BRAND ID"
               })
           }
       })
   }else {
       res.status(400).json({
           error: "Please Provide BRAND ID"
       })
   }
}

exports.get_models = (req, res, next) => {
   if(req.query.car_id){
       models.car.findOne({where: {id: req.query.car_id}}).then(car => {
           if(car){
               
       models.model.findAll({where: {car_id: req.query.car_id}}).then(models => {
           res.json({
               car,
               models
           })
       })
           }else {
               res.status(404).json({
                   error: "Invalid Car ID"
               })
           }
       })
   }else {
       res.status(400).json({
           error: "Please Provide Car ID"
       })
   }
}

exports.get_products = (req, res, next) => {
    var limit = req.query.limit && typeof req.query.limit == "number" ? parseInt(req.query.limit) : 50
    if(req.query.model_id){
     models.part.findAndCountAll({where: {model_id: req.query.model_id}, limit: limit, order: [["createdAt", "DESC"]], include: ["reviews"]}).then(prods => {
        res.json({
            limit: limit,
            products: prods.rows,
            total: prods.count,
            showing: prods.rows.length
        })
           })
    }else {
          models.part.findAndCountAll({limit: limit, order: [["createdAt", "DESC"]], include: ["reviews"]}).then(prods => {
        res.json({
            limit: limit,
            products: prods.rows,
            total: prods.count,
            showing: prods.rows.length
        })
           })   
    }

}


exports.get_cat_products = (req, res, next) => {
    if(req.query.id){
    models.cat.findOne({where: {id: req.query.id}}).then(cat => {
        if(!cat){
            return res.status(404).json({
                error: "Category Not Found"
            })
        }else {
                var limit = req.query.limit && typeof req.query.limit == "number" ? parseInt(req.query.limit) : 50
     models.part.findAndCountAll({where: {category: cat.slug}, limit: limit, order: [["createdAt", "DESC"]], include: ["reviews"]}).then(prods => {
        res.json({
            limit: limit,
            products: prods.rows,
            total: prods.count,
            showing: prods.rows.length
        })
           })

        }
    })

    }else {
        res.status(400).json({
            error: "Please Provide Category ID"
        })
    }
}

exports.get_product = (req, res, next) => {
    if(req.body.id){
            models.part.findOne({where: {id: req.body.id}, include: ["maker", {association: "model", as: "model", include: [{association: "car", as: "car", include: ["brand"]}]}, {association: "reviews", as: "reviews", include: ["user"]}]}).then(part => {
            if(part){
                 models.part.findAll({limit: 5, order: [["createdAt", "DESC"]]}).then(newProd => {
                     
            res.json({
                product: part,
                recent_products: newProd
            })
            
                 })
        }else {
        res.status(404).send("No Product found with this ID")
        }
    })      
    }else {
        res.status(400).json({
            error: "Please Provide Product ID"
        })
    }
}


exports.get_user = (req, res, next) => {
     jwt.verify(req.query.token, 'eLFQivL/gdEiW0iuZY1YgdYfF1U=@!', (err, data) => {
    if(err){
    res.status(400).json({
    error: "Please ensure you've provided a valid token"
    })
    }
    
    else {
    models.user.scope('withoutPassword').findOne({where: {id: data.user.id}}).then(user => {
        res.json({
            user
        })
    })
    }
    })
}

exports.get_items = (req, res, next) => {
    if(req.body.products.length > 0){
        var items = []
        req.body.products.forEach((product, id, arr) => {
        if(id + 1 == arr.length){    
    models.part.findOne({where: {id: product.id}, include: [{association: "model", as: "model", include: [{association: "car", as: "car", include: ["brand"]}]}]}).then(item => {
        if(item){
        items.push(item)
        res.json({
            items
        })
        }
        else {
            res.json({
                error: "No Items"
            })
        }
        })
        }else {
             models.part.findOne({where: {id: product.id}, include: [{association: "model", as: "model", include: [{association: "car", as: "car", include: ["brand"]}]}]}).then(item => {
        if(item){
        items.push(item)
        }
        })   
        }
        })
    }else {
        res.json({
            items: []
        })
    }
}

exports.order = (req, res, next) => {
     jwt.verify(req.query.token, 'eLFQivL/gdEiW0iuZY1YgdYfF1U=@!', (err, data) => {
    if(err){
    res.status(400).json({
    error: "Please ensure you've provided a valid token"
    })
    }
    
    else {
    models.user.findOne({where: {id: data.user.id}}).then(user => {
    if(user){
           if(req.body.items && req.body.items.length > 0 && req.body.payment_id && req.body.type  && req.body.amount && req.body.comment){
       
        if(req.body.type == "delivery"){
                    models.order.create({user_id:  user.id, state: null, delivery_address: req.body.address || "null", total_paid: req.body.amount, type: "delivery", status: "pending", txn_id: req.body.payment_id, commment: req.body.comment, shipping_cost: req.body.ship}).then(order => {
                       req.body.items.forEach((item, id)=> {
                            models.part.findOne({where: {id: item.id}}).then(part => {
                        if(id + 1 == req.body.items.length){
                             models.item.create({order_id: order.id, part_id: item.id, amount: item.amount, quantity: item.quantity}).then(itm => {
                                 models.part.update({stock: parseInt(part.stock) - parseInt(item.quantity)},{where: {id: part.id}})
                                 res.json({
                                     message: "order placed successfully",
                                     order: order
                                 })
                             })
                        }else {
                           models.item.create({order_id: order.id, part_id: item.id, amount: item.amount, quantity: item.quantity})
                        }
                            })
                       })
                        })
                }
                if(req.body.type == "pickup"){
                    models.order.create({user_id: user.id, state: null, delivery_address: req.body.address || "null", total_paid: req.body.amount, type: "pickup", status: "pending", txn_id: req.body.payment_id, commment: req.body.comment, shipping_cost: req.body.ship}).then(order => {
                       req.body.items.forEach((item, id)=> {
                            models.part.findOne({where: {id: item.id}}).then(part => {
                        if(id + 1 == req.body.items.length){
                             models.item.create({order_id: order.id, part_id: item.id, amount: item.amount, quantity: item.quantity}).then(itm => {
                                 models.part.update({stock: parseInt(part.stock) - parseInt(item.quantity)},{where: {id: part.id}})
                                 res.json({
                                     message: "order placed successfully",
                                     order: order
                                 })
                             })
                        }else {
                           models.item.create({order_id: order.id, part_id: item.id, amount: item.amount, quantity: item.quantity})
                        }
                            })
                            })
                        })
                }                
       
    }else {
        res.json({
            error: "Please Provide full details"
        })
    } 
    }else {
        res.status(400).json({
            error: "Invalid Token Provided"
        })
    }
    })
    }
    })

}

exports.get_withdrawals = (req, res, next) => {
    jwt.verify(req.query.token, 'eLFQivL/gdEiW0iuZY1YgdYfF1U=@!', (err, data) => {
    if(err){
    res.status(400).json({
    error: "Please ensure you've provided a valid token"
    })
    }
    
    else {
    models.user.findOne({where: {id: data.user.id}}).then(user => {
        if(user){
             models.withdraw.findAll({where: {user_id: user.id}}).then(withdraws => {
                res.json(({
                    history: withdraws
                }))  
            })
        }else {
            res.status(400).json({
                error: "Invalid Token Provided"
            })
        }
    })
    }
    })
}

exports.get_orders = (req, res, next) => {
    jwt.verify(req.query.token, 'eLFQivL/gdEiW0iuZY1YgdYfF1U=@!', (err, data) => {
    if(err){
    res.status(400).json({
    error: "Please ensure you've provided a valid token"
    })
    }
    
    else {
    models.user.findOne({where: {id: data.user.id}}).then(user => {
        if(user){
             models.order.findAll({where: {user_id: user.id}, include: [{association: "items", as: "items", include: [{association: "part", as: "part", include: [{association: "model", as: "model", include: ["car"]}]}]}]}).then(orders => {
                res.json(({
                    orders: orders
                }))  
            })
        }else {
            res.status(400).json({
                error: "Invalid Token Provided"
            })
        }
    })
    }
    })
}

exports.get_order = (req, res, next) => {
    jwt.verify(req.query.token, 'eLFQivL/gdEiW0iuZY1YgdYfF1U=@!', (err, data) => {
    if(err){
    res.status(400).json({
    error: "Please ensure you've provided a valid token"
    })
    }
    
    else {
        if(req.body.order_id){
             models.order.findOne({where: {id: req.body.order_id}, include: [{association: "items", as: "items", include: [{association: "part", as: "part", include: [{association: "model", as: "model", include: ["car"]}]}]}]}).then(order => {
                res.json(({
                    order: order
                }))  
            })
        }else {
            res.status(400).json({
                error: "Please provide order ID"
            })
        }
    }
    })
}

exports.request_withdrawal = (req, res, next) => {
        jwt.verify(req.query.token, 'eLFQivL/gdEiW0iuZY1YgdYfF1U=@!', (err, data) => {
    if(err){
    res.status(400).json({
    error: "Please ensure you've provided a valid token"
    })
    }
    
    else {
    models.user.findOne({where: {id: data.user.id}}).then(user => {
        if(user){
if(req.body.points && req.body.bank_name && req.body.acct_no && req.body.name){
        models.setting.findAll().then(settings => {
            if(user.bal > parseInt(req.body.points) || user.bal == parseInt(req.body.points)){
              if(parseInt(req.body.points) < 50){
              return res.status(400).json({
                      error: "Minimum Withdrawal is 50 POINTS."
                  })
              }
        models.withdraw.create({
            amount: parseInt(req.body.points) * settings[0].points,
            points: req.body.points,
            bank: req.body.bank_name,
            account_name: req.body.name,
            account_no: req.body.acct_no,
            user_id: user.id
        }).then(wit => {
            models.user.update({bal: user.bal - parseInt(req.body.points)}, {where: {id: user.id}})
            res.json({
                message: "Withdrawal request recieved and is being processed",
                amount: "NGN " + parseInt(req.body.points) * settings[0].points
            })
        })  
            }else {
                res.status(400).json({
                    error: "Insufficient Points"
                })
            }
            
        })
    }else {
        res.json({
            error: "Please Provide Full Details"
        })
    }
    
    
        }else {
            res.status(400).json({
                error: "Invalid Token Provided"
            })
        }
    })
    }
    })
}
exports.get_shipping = (req, res, next) => {
        if(req.body.lat && req.body.long){
        var lat_main = 9.0710391
        var long_main = 7.437945300000001
        models.amountperkm.findOne().then(amount => {
            var amt = parseInt(amount.price)
            var dist = geodist({lat: lat_main, lon: long_main}, {lat: req.body.lat, lon: req.body.long}, {exact: true, unit: 'km'})
            if(dist){
                res.json({
                    amountperkm: amt,
                    dist,
                    distance: dist + " KM",
                    cost: dist * amnt,
                    message: "Shipping Cost Calculated"
                })
            }else {
                res.json({
                    error: "There was an error with that request"
                })
            }
        })
    }else {
        res.json({
            error: "Please Provide a valid address"
        })
    }
}
exports.forgot_password = (req, res, next) => {
    if(req.body.email){
        models.user.findOne({where: {email: req.body.email}}).then(user => {
            if(user){
                var token = cryptoRandomString({length: 6, type: 'numeric'})
                models.user.update({
                    token
                }, {where: {id: user.id}}).then(rows => {

                    res.json({
                        message: "Check your email for a password reset token"
                    })
                       sendMail(user.email, "Password", `Hi ${user.firstName},You have requested to change your password on GAPAAUTOPARTS your one time token is ${token}`)
                    sendSMS(user.phone, `Hi ${user.firstName},You have requested to change your password on GAPAAUTOPARTS your one time token is ${token}`)
                        			
                })
            }else {
                res.status(401).json({
                    error: "No account was found associated to this email"
                })
            }
        })
    }else {
        
    }
}
exports.reset_pass = (req, res, next) => {
    if(req.body.pass && req.body.pass2 && req.body.token){
        var tok = req.body.token
        if(req.body.pass != req.body.pass2){
         res.status(400).json({
             error: "Passwords do not match"
         })   
        }
        else {
                   let errors = {}
                   return validateChange(errors, req).then(errors => {
                        		if(!isEmpty(errors)){
                        			res.status(400).json(errors)
                        		}
                        		
                        		
                        		else{
                        		     models.user.findOne({where: {token: tok}}).then(usr => {
                        		         if(!usr){
                        		             return res.status(400).json({
                        		                 error: "Invalid Token Provided"
                        		             })
                        		         }
                        		            models.user.update({password: bcrypt.hashSync(req.body.pass, bcrypt.genSaltSync(10))}, {where: {id: usr.id}}).then(rows => {
                        		      models.user.update({token: "******"}, {where: {id: usr.id}})
                        		          res.json({
                        		              message: "Password Reset succesfully"
                        		          })
                        		      })
                        		     })
                        		   
                        		    
                        		}
                        		
                        		
                        		})
        }
    }
    else {
      res.status(400).json({
          error: "Please Provide Password reset token, New password and password confirmation"
      })  
    }
}

exports.search_products = (req, res, next) => {
    if(req.body.title){
        if(req.body.cat){
            var query = {
                name: Sequelize.where(Sequelize.fn('LOWER', Sequelize.col('name')), 'LIKE', '%' + req.body.title + '%'),
                category: req.body.cat
            }
        }else {
             var query = {
                name: Sequelize.where(Sequelize.fn('LOWER', Sequelize.col('name')), 'LIKE', '%' + req.body.title + '%')
            }
        }
        models.part.findAll({
            limit: 100,
            where: query
        }).then(names => {
            if(names){
               res.json({
                data: names
            }) 
            }else {
        res.status(404).json({
            error: "No data found"
        })
            }
        })
    }else {
        res.status(404).json({
            error: "Please Provide an input"
        })
    }
}

exports.update_profile = (req, res, next) => {
    jwt.verify(req.query.token, 'eLFQivL/gdEiW0iuZY1YgdYfF1U=@!', (err, data) => {
    if(err){
    res.status(400).json({
    error: "Please ensure you've provided a valid token"
    })
    }
    
    else {
models.user.findOne({where: {id: data.user.id}}).then(user => {
    if(user){
            if(req.body.fname && req.body.lname && req.body.address){
        models.user.update({
            firstName: req.body.fname,
            lastName: req.body.lname,
            address: req.body.address
        }, {where: {
            id: data.user.id
        }}).then(rows => {
            res.json({
                message: "Profile Updated"
            })
        })
    }else {
        res.json({
            error: "Please Provide Firstname and LastName and Address",
            body: req.body
        })
    }
    }else {
        res.status(404).json({
        error: "Invalid TOKEN Provided"
        })
    }
})
    }
    })
}

exports.update_dp = (req, res, next) => {
    jwt.verify(req.query.token, 'eLFQivL/gdEiW0iuZY1YgdYfF1U=@!', (err, data) => {
    if(err){
    res.status(400).json({
    error: "Please ensure you've provided a valid token"
    })
    }
    
    else {
models.user.findOne({where: {id: data.user.id}}).then(user => {
    if(user){
        console.log(user)
         var form = new formidable.IncomingForm(); 
    form.parse(req, function(err, fields, files){
        var __parentDir = "/home/gapaautoparts/public_html/app/public/"
        var oldPath = files.image.path; 
        var newPath = __parentDir + "uploads" + '/'+ user.id + files.image.name 
        var rawData = fs.readFileSync(oldPath) 
      
        fs.writeFile(newPath, rawData, function(err){ 
            if(err) {res.json({error: "there was an error", err})}
            
               else{
                 
          models.user.update({img_url: "/uploads/" + user.id + files.image.name }, {where: {id: user.id}}).then(user => {
                res.json({message: "Profile Image Updated succesfully"}); 
        })  
               }
        }) 
  }) 
    }else {
        res.status(404).json({
        error: "Invalid TOKEN Provided"
        })
    }
})
    }
    })
}

exports.rate = (req, res, next) => {
    jwt.verify(req.query.token, 'eLFQivL/gdEiW0iuZY1YgdYfF1U=@!', (err, data) => {
    if(err){
    res.status(400).json({
    error: "Please ensure you've provided a valid token"
    })
    }
    
    else {
models.user.findOne({where: {id: data.user.id}}).then(user => {
    if(user){
        if(req.body.review && req.body.part_id && req.body.rating){
            models.review.findOne({where: {user_id: user.id, part_id: req.body.part_id}}).then(rev => {
                if(rev){
                    return res.status(400).json({
                        message: "You have already rated this Product"
                    })
                }
        models.review.create({user_id: user.id, part_id: req.body.part_id, review: req.body.review, rating: req.body.rating}).then(review => {
            res.json({
                message: "Review Sent"
            })
        })
            })
    }else {
        res.status(400).json({
            error: "Please Provide Full Details"
        })
    }
    }else {
        res.status(404).json({
        error: "Invalid TOKEN Provided"
        })
    }
})
    }
    })
}


    function verif() { 
    jwt.verify(req.token, 'eLFQivL/gdEiW0iuZY1YgdYfF1U=@!', (err, data) => {
    if(err){
    res.status(400).json({
    error: "Please ensure you've provided a valid token"
    })
    }
    
    else {
    models.maintenance_cat.findAll().then(cats => {
    	res.json({
        	categories: cats
        })
    })
    }
    })
}